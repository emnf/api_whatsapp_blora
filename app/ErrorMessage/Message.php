<?php

namespace App\ErrorMessage;
/*
|--------------------------------------------------------------------------
| API Message
|--------------------------------------------------------------------------
|
| Here is where you can View Message Of Api Bank, Hal yang paling terpenting
| didalam Hidup ini adalah Ngopi!!
|
| Description of ApiBank
|
| @author Frennandi ade
|
*/

use App\Models\datah2h\h2hLogModel;

class Message
{
    // 00
    public static function successUsername($username, $token)
    {
        $response = [
            'data' => [
                "username" => $username,
                'current_token' => $token
            ],
            "responsecode" => "200",
            "responsemessage" => "Login success"
        ];
        Message::saveLog($response['responsecode'], $response['responsemessage'] . ' | ' . $username . ' | ' . $token, 'login');
        return $response;
    }

    public static function successLogout()
    {

        $res = [
            "responsecode" => "00",
            "responsemessage" => "You have successfully logged out"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'], 'logout');
        return $res;
    }

    public static function reversalSuccess($response, $data)
    {
        $res = [
            "data" => $response,
            "responsecode" => "00",
            "responsemessage" => "Success"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $data, 'reversal');
        return $res;
    }

    public static function successInquiryPayment($response)
    {
        $res = [
            "data" => $response,
            "responsecode" => "00",
            "responsemessage" => "success"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . ($response['idbilling'] ?? $response['nop']), 'getdata');
        return $res;
    }

    public static function successCekTagihan($response)
    {
        $res = [
            "data" => $response,
            "responsecode" => "00",
            "responsemessage" => "success"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'], 'getdata');
        return $response;
    }

    public static function successTrackPermohonanBphtb($response)
    {
        $res = [
            "data" => $response,
            "responsecode" => "00",
            "responsemessage" => "success"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'], 'getdata');
        return $response;
    }


    // 01
    public static function dataSptNotFound($response)
    {
        $res = [
            "responsecode" => "01",
            "responsemessage" => "Data Tidak Ditemukan"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $response, 'getdata');
        return $res;
    }

    // 02
    public static function paymentPaid($response)
    {
        $res = [
            "responsecode" => "02",
            "responsemessage" => "Tagihan Sudah Lunas"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $response, 'getdata');
        return $res;
    }

    // 03
    public static function paymentNotSame($response)
    {
        $res = [
            "responsecode" => "03",
            "responsemessage" => "Error Nominal Tagihan Tidak Sesuai"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $response, 'getdata');
        return $res;
    }

    // 04
    public static function refnumNotSame($response)
    {
        $res = [
            "responsecode" => "04",
            "responsemessage" => "Data Pembayaran Tidak Ditemukan"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $response, 'getdata');
        return $res;
    }

    // 05
    public static function invalidUsername()
    {
        $response = [
            "data" => [],
            "responsecode" => "403",
            "responsemessage" => "Login failed"
        ];

        Message::saveLog($response['responsecode'], $response['responsemessage'], 'login');
        return $response;
    }

    // 06
    public static function dataRequestInvalid($response)
    {
        $res = [
            "responsecode" => "06",
            "responsemessage" => "Format Body Salah"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $response, 'getdata');
        return $res;
    }

    // 07
    public static function invalidSave($response)
    {
        $res = [
            "responsecode" => "07",
            "responsemessage" => "Failed Save DB Error"
        ];

        Message::saveLog($res['responsecode'], $res['responsemessage'] . ' | ' . $response, 'DB_ERROR');
        return $res;
    }

    // 401
    public static function tokenInvalid()
    {
        $response = [
            "data" => [
                "responsemessage" => "Your token is invalid or expired, Bad Request!!"
            ],
            "responsemessage" => "ACCESS FAILED",
            "responsecode" => "401"
        ];

        Message::saveLog('401', 'Invalid Token', 'getdata');
        return $response;
    }


    // save log
    public static function saveLog($responsecode, $message, $url)
    {
        h2hLogModel::create([
            "kode" => $responsecode,
            "message" => $message,
            'url' => $url,
            'akses' => auth()->user()->username ?? ''
        ]);
    }
}
