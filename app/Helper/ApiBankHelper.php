<?php

namespace App\Helper;

use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| API Message
|--------------------------------------------------------------------------
|
| Here is where you can View Helper Of Api Bni, Hal yang paling terpenting
| didalam Hidup ini adalah Ngopi!!
|
| Description of ApiBni
|
| @author Frennandi ade
|
*/

class ApiBankHelper
{
    public static function createNTPD($nourutKodeBayar)
    {
        $ntpd = "01" . date('ym') . str_pad($nourutKodeBayar, 5, "0", STR_PAD_LEFT);
        return $ntpd;
    }

    /* tanpa titik */
    public static function explodeNop($data)
    {
        return $data = [
            'KD_PROPINSI' => substr($data, 0, 2),
            'KD_DATI2' => substr($data, 2, 2),
            'KD_KECAMATAN' => substr($data, 4, 3),
            'KD_KELURAHAN' => substr($data, 7, 3),
            'KD_BLOK' => substr($data, 10, 3),
            'NO_URUT' => substr($data, 13, 4),
            'KD_JNS_OP' => substr($data, 17, 1),
            'THN_PAJAK_SPPT' => substr($data, 18, 4),
            'NOP' => substr($data, 0, 18)
        ];
    }

    public static function explodeNopermohonan($data)
    {
        return $data = [
            'THN_PELAYANAN' => substr($data, 0, 4),
            'BUNDEL_PELAYANAN' => substr($data, 4, 4),
            'NO_URUT_PELAYANAN' => substr($data, 8, 3)
        ];
    }

    public static function formatnpwpd($data)
    {
        $part1 = substr($data, 0, 1);
        $part2 = substr($data, 1, 1);
        $part3 = substr($data, 2, 7);
        $part4 = substr($data, 9, 2);
        $part5 = substr($data, 11, 2);

        $output = $part1 . "." . $part2 . "." . $part3 . "." . $part4 . "." . $part5;
        return $output;
    }

    public static function dendaPbb($tgl, $bayar)
    {
        $tempo = Carbon::parse($tgl); // jatuh tempo
        if ($tempo->gt(Carbon::now()->addDay(-1)->subMinutes(10))) { // cek kalo bayarnya masih belom sampe tgl denda
            return [
                'jarak' => 0,
                'total' => $bayar * 1,
                'denda' => 0
            ];
        };

        $jarak = $tempo->diffInMonths(Carbon::now()); // ini ngitung jarak bulan jatuh tempo dan bayar
        if ($jarak == 0) { // atasi tgl jatuh tempo sama bulan tgl bayar
            $jarak = 1;
        } else {
            $jarak = ($jarak > 24) ? 24 : $jarak + 1; // max bulan sampe 24 atau denda 48%
        }

        $denda = round(($jarak * 2 / 100) * $bayar); // ini yaa tau lahh ngitungnya
        $total = $denda + $bayar;
        return [
            'jarak' => $jarak,
            'total' => $total,
            'denda' => $denda
        ];
    }

    public static function dendaPajakLain($tgl, $bayar)
    {
        $tempo = Carbon::parse($tgl); // jatuh tempo
        if ($tempo->gt(Carbon::now()->addDay(-1)->subMinutes(10))) { // cek kalo bayarnya masih belom sampe tgl denda
            return [
                'jarak' => 0,
                'total' => $bayar * 1,
                'denda' => 0
            ];
        };

        $jarak = $tempo->diffInMonths(Carbon::now()); // ini ngitung jarak bulan jatuh tempo dan bayar
        if ($jarak == 0) { // atasi tgl jatuh tempo sama bulan tgl bayar
            $jarak = 1;
        } else {
            $jarak = ($jarak > 15) ? 15 : $jarak + 1; // max bulan sampe 15 atau denda 48%
        }

        $denda = round(($jarak * 2 / 100) * $bayar); // ini yaa tau lahh ngitungnya
        $total = $denda + $bayar;
        return [
            'jarak' => $jarak,
            'total' => $total,
            'denda' => $denda
        ];
    }
}
