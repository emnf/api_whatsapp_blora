<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ErrorMessage\Message;
use App\Models\User;
use Validator;
use Auth;

/*
|--------------------------------------------------------------------------
| API Controller Auth
|--------------------------------------------------------------------------
|
| Login, Logout sama generate bearer disini gan
|
| Description of ApiBank
|
| @author Frennandi ade
|
*/

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        if (!Auth::attempt($request->only('username', 'password'))) {
            return response()->json(Message::invalidUsername(), 401);
        }

        $user = User::where('username', $request['username'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json(Message::successUsername($request['username'], $token));
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return response()->json(Message::successLogout());
    }
}
