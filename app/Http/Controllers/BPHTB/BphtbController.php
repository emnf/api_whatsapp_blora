<?php

namespace App\Http\Controllers\BPHTB;

use App\ErrorMessage\Message;
use App\Http\Controllers\Controller;
use App\Http\Traits\Bphtb\PermohonanBphtbTrait;
use App\Models\databphtb\SptBphtbModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BphtbController extends Controller
{
    use PermohonanBphtbTrait;
    public function permohonanByKodeDaftar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nodaftar' => 'required|string',
            'tahundaftar' => 'required|string|max:4'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['nodaftar'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->permohonan($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound($request['nodaftar'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function permohonanByStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|string',
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['nodaftar'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->getPermohonanByStatus($request->all());
            // dd($permohonan->count());
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {
            // dd($th);
            abort(response()->json(Message::dataSptNotFound($request['status'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function permohonanByBilling(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'billing' => 'required|string',
        ]);
        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['billing'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->getPermohonanByBilling($request->all());
            $permohonan['idbilling'] = $request['billing'];
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound($request['billing'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function permohonanByDibayar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dibayar' => 'required|string',
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);
        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->getPermohonanByDibayar($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {

            abort(response()->json(Message::dataSptNotFound($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
    }
}
