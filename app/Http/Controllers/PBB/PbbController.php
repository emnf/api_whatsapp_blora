<?php

namespace App\Http\Controllers\PBB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ErrorMessage\Message;
use App\Http\Traits\Pbb\TagihanPbbTrait;
use App\Models\datapbb\DatPetaBlok;
use App\Models\datapbb\RefKecamatan;
use App\Models\datapbb\RefKelurahan;

class PbbController extends Controller
{
    use TagihanPbbTrait;
    public function cekTagihan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nop' => 'required|string|max:18'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['nop'] . ' | ' . $validator->errors()->first()), 200));
        }

        try {
            $tagihan = $this->tagihan($request->all());
            return response()->json(Message::successCekTagihan($tagihan), 200);
        } catch (\Throwable $th) {
            dd($th);
            abort(response()->json(Message::dataSptNotFound($request['nop'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function statuspelayanan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nopermohonan' => 'required|string|max:18'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['nopermohonan'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $tagihan = $this->pelayananPst($request['nopermohonan']);
            return response()->json(Message::successTrackPermohonanBphtb($tagihan), 200);
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound($request['nopermohonan'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function bystatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|string|max:18',
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['status'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $tagihan = $this->pelayananPstbyStatus($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($tagihan), 200);
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound($request['nopermohonan'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function cekTagihanWilayah(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tahun' => 'required|string|max:4',
            'kecamatan' => 'required|string',
            'kelurahan' => 'string',
            'status' => 'required|string',
            'kdblok' => 'required|string'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['kecamatan'] . ' | ' . $validator->errors()->first()), 200));
        }


        try {
            $tagihan = $this->tagihankeckel($request->all());
            // dd($tagihan);
            return response()->json(Message::successCekTagihan($tagihan), 200);
        } catch (\Throwable $th) {
            // dd($th);
            abort(response()->json(Message::dataSptNotFound($request['kecamatan'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function kecamatan(Request $request)
    {
        try {
            $kecamatan = RefKecamatan::get();
            foreach ($kecamatan as $key => $value) {
                $data = [
                    'kdkecamatan' => $value->kd_kecamatan,
                    'nmkecamatan' => $value->nm_kecamatan
                ];
                $res[] = $data;
            }

            return $res;
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound(000), 200));
        }
    }

    public function kelurahan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kecamatan' => 'required|string|max:18'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['kecamatan'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $kecamatan = RefKecamatan::where('NM_KECAMATAN', $request['kecamatan'])->first();
            $kelurahan = RefKelurahan::where('KD_KECAMATAN', $kecamatan->kd_kecamatan)->get();

            foreach ($kelurahan as $key => $value) {
                $data = [
                    'kdkelurahan' => $value->kd_kelurahan,
                    'nmkelurahan' => $value->nm_kelurahan
                ];
                $res[] = $data;
            }

            return $res;
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound(000), 200));
        }
    }

    public function kdblok(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kdkecamatan' => 'required|string|max:18',
            'kdkelurahan' => 'required|string|max:18'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['kdkecamatan'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $datblok = DatPetaBlok::where('KD_KECAMATAN', $request['kdkecamatan'])->where('KD_KELURAHAN', $request['kdkelurahan'])->get();

            foreach ($datblok as $key => $value) {
                $data = [
                    'kdblok' => $value->kd_blok
                ];
                $res[] = $data;
            }

            return $res;
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound(000), 200));
        }
    }

    public function statusbayar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'thnpajak' => 'required|string|max:18',
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['thnpajak'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $tagihan = $this->statusByr($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($tagihan), 200);
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound($request['thnpajak'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function pembayarnpertama(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'thnpajak' => 'required|string|max:18',
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['thnpajak'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $tagihan = $this->pembayaranPertama($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($tagihan), 200);
        } catch (\Throwable $th) {
            abort(response()->json(Message::dataSptNotFound($request['thnpajak'] . ' | ' . $validator->errors()->first()), 200));
        }
    }
}
