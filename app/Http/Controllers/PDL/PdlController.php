<?php

namespace App\Http\Controllers\PDL;

use App\ErrorMessage\Message;
use App\Http\Controllers\Controller;
use App\Http\Traits\Pdl\PdlTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PdlController extends Controller
{
    use PdlTrait;
    public function cekTagihan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'npwpd' => 'required|string|max:18',
            'tahun' => 'string|required|max:4'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['status'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $tagihan = $this->tagihan($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($tagihan), 200);
        } catch (\Throwable $th) {
            // dd($th);
            abort(response()->json(Message::dataSptNotFound($request['nopermohonan'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function cekTagihanPeridoe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dibayar' => 'required|string',
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->tagihanPeriode($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {
            dd($th);
            abort(response()->json(Message::dataSptNotFound($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function cekTagihanTglPendataan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->tagihanBytglInput($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {

            abort(response()->json(Message::dataSptNotFound($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
    }

    public function cekTagihanTglBayar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'datefrom' => 'date|required',
            'dateto' => 'date|required'
        ]);

        if ($validator->fails()) {
            abort(response()->json(Message::dataRequestInvalid($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
        try {
            $permohonan = $this->tagihanBytglBayar($request->all());
            return response()->json(Message::successTrackPermohonanBphtb($permohonan), 200);
        } catch (\Throwable $th) {
            // $config = config('database.connections.simpatda');

            // Extract the 'host' value from the configuration
            // $host = $config['host'];
            // dd($host);
            abort(response()->json(Message::dataSptNotFound($request['dibayar'] . ' | ' . $validator->errors()->first()), 200));
        }
    }
}
