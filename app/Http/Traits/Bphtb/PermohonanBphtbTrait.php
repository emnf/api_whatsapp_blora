<?php

namespace App\Http\Traits\Bphtb;

use App\ErrorMessage\Message;
use App\Helper\ApiBankHelper;
use App\Models\databphtb\PejabatBphtbModel;
use App\Models\databphtb\PembayaranBphtbModel;
use App\Models\databphtb\SptBphtbModel;
use App\Models\databphtb\UserBphtbModel;

trait PermohonanBphtbTrait
{
    public function permohonan($request)
    {
        $data = SptBphtbModel::where('t_kohirspt', $request['nodaftar'])->where('t_periodespt', $request['tahundaftar'])
            ->leftJoin('t_detailsptbphtb', 't_detailsptbphtb.t_idspt', 't_spt.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', 't_spt.t_idjenistransaksi')
            ->leftJoin('t_pembayaranspt', 't_pembayaranspt.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt', '=', 't_pembayaranspt.t_idpembayaranspt')
            ->first();

        $pembayaran = PembayaranBphtbModel::where('t_idspt', $data->t_idspt)->first();

        $bphtb = $data->t_totalspt;
        if ($data->p_idpemeriksaan != null) {
            $bphtb = $data->p_totalspt;
            if ($data->t_totalspt < $data->p_totalspt) {
                $bphtb = $data->p_totalspt;
            } else {
                $bphtb = $data->t_totalspt;
            }
        }

        $data1 = [
            'id_transaksi' => $request['nodaftar'],
            'jenis_transaksi' => $data->s_namajenistransaksi,
            'nop' => str_replace('.', '', $data->t_nopbphtbsppt),
            'petugas' => null,
            'nama_wp' => $data->t_namawppembeli,
            'telp_wp' => $data->t_telponwppembeli,
            'riwayat' => 'Transaksi Didaftarkan',
            'tanggal' => $data->t_tglprosesspt,
            'tanggal_diperbarui' => $data->t_tglprosesspt,
            'status' => 'Diperiksa',
            'tgl_dibayar' => null,
            'jatuh_tempo' => null,
            'idbilling' => $pembayaran->t_kodebayarbanksppt,
            'bphtb' => $bphtb ?? $data->t_totalspt
        ];
        $res = [];
        $res[] = $data1;
        $verifikasiBerkas = null;
        $verifikasiSPT = null;
        $user = UserBphtbModel::where('s_iduser', $pembayaran->t_pejabatverifikasiberkas)->first();

        if ($pembayaran) {
            if ($pembayaran->t_tglverifikasiberkas == null) {
                $verifikatorBerkas = PejabatBphtbModel::where('s_idpejabat', $user->s_idpejabat_idnotaris)->first();

                $verifikasiBerkas = [
                    'id_transaksi' => $request['nodaftar'],
                    'jenis_transaksi' => $data->s_namajenistransaksi,
                    'nop' => str_replace('.', '', $data->t_nopbphtbsppt),
                    'petugas' => $verifikatorBerkas->s_namapejabat ?? null,
                    'nama_wp' => $data->t_namawppembeli,
                    'telp_wp' => $data->t_telponwppembeli,
                    'riwayat' => 'Verifikasi Berkas',
                    'tanggal' => $data->t_tglprosesspt,
                    'tanggal_diperbarui' => $pembayaran->t_tglverifikasiberkas,
                    'status' => 'Diverifikasi',
                    'tgl_dibayar' => null,
                    'jatuh_tempo' => null,
                    'idbilling' => $pembayaran->t_kodebayarbanksppt,
                    'bphtb' => $bphtb ?? $data->t_totalspt
                ];
                $res[] = $verifikasiBerkas;
            }
            if ($pembayaran->t_tglverifikasispt != null) {
                $userSpt = UserBphtbModel::where('s_iduser', $pembayaran->t_pejabatverifikasispt)->first();
                $verifikatorSPT = PejabatBphtbModel::where('s_idpejabat', $userSpt->s_idpejabat_idnotaris)->first();
                $verifikasiSPT = [
                    'id_transaksi' => $request['nodaftar'],
                    'jenis_transaksi' => $data->s_namajenistransaksi,
                    'nop' => str_replace('.', '', $data->t_nopbphtbsppt),
                    'petugas' => $verifikatorSPT->s_namapejabat ?? null,
                    'nama_wp' => $data->t_namawppembeli,
                    'telp_wp' => $data->t_telponwppembeli,
                    'riwayat' => 'Transaksi divalidasi oleh Petugas dan menunggu pembayaran',
                    'tanggal' => $data->t_tglprosesspt,
                    'tanggal_diperbarui' => $pembayaran->t_tglverifikasispt,
                    'status' => 'Divalidasi',
                    'tgl_dibayar' => null,
                    'jatuh_tempo' => null,
                    'idbilling' => $pembayaran->t_kodebayarbanksppt,
                    'bphtb' => $bphtb ?? $data->t_totalspt
                ];
                $res[] = $verifikasiSPT;
            }

            if ($pembayaran->t_tanggalpembayaran != null) {
                $userSpt = UserBphtbModel::where('s_iduser', $pembayaran->t_pejabatverifikasispt)->first();
                $verifikatorSPT = PejabatBphtbModel::where('s_idpejabat', $userSpt->s_idpejabat_idnotaris)->first();
                $pembayaranSpt = [
                    'id_transaksi' => $request['nodaftar'],
                    'jenis_transaksi' => $data->s_namajenistransaksi,
                    'nop' => str_replace('.', '', $data->t_nopbphtbsppt),
                    'petugas' => $verifikatorSPT->s_namapejabat ?? null,
                    'nama_wp' => $data->t_namawppembeli,
                    'telp_wp' => $data->t_telponwppembeli,
                    'riwayat' => 'Transaksi telah lunas pada tanggal : ' . date('d-m-Y', strtotime($pembayaran->t_tanggalpembayaran)),
                    'tanggal' => $data->t_tglprosesspt,
                    'tanggal_diperbarui' => $pembayaran->t_tanggalpembayaran,
                    'status' => 'Selesai',
                    'tgl_dibayar' => $pembayaran->t_tanggalpembayaran,
                    'jatuh_tempo' => null,
                    'idbilling' => $pembayaran->t_kodebayarbanksppt,
                    'bphtb' => $bphtb ?? $data->t_totalspt
                ];
                $res[] = $pembayaranSpt;
            }
        }

        return $res;
    }

    public function getPermohonanByStatus($request)
    {
        $tgl0 = date('Y-m-d', strtotime($request['datefrom']));
        $tgl1 = date('Y-m-d', strtotime($request['dateto']));

        $data = SptBphtbModel::select(
            't_spt.*',
            't_detailsptbphtb.t_namawppembeli',
            't_detailsptbphtb.t_telponwppembeli',
            's_jenistransaksi.s_namajenistransaksi',
            't_pembayaranspt.t_tglverifikasiberkas',
            't_pembayaranspt.t_tglverifikasispt',
            't_pembayaranspt.t_tanggalpembayaran',
            't_pembayaranspt.t_kodebayarbanksppt',
            's_notaris.s_namanotaris',
            't_pemeriksaan.p_totalspt',
            't_pemeriksaan.p_idpemeriksaan'
        )
            ->leftJoin('t_detailsptbphtb', 't_detailsptbphtb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->leftJoin('t_pembayaranspt', 't_pembayaranspt.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_users', 's_users.s_iduser', '=', 't_spt.t_idnotarisspt')
            // ->leftJoin('s_notaris', 's_notaris.s_idnotaris', '=', 's_users.s_idpejabat_idnotaris::int')
            ->leftJoin('s_notaris', 's_notaris.s_idnotaris', '=', \DB::raw('CAST(s_users.s_idpejabat_idnotaris AS INTEGER)'))
            ->leftJoin('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt', '=', 't_pembayaranspt.t_idpembayaranspt')
            // ->where('t_periodespt', '=', $tahunSpt)
        ;
        switch ($request['status']) {
            case 1:
                $data = $data
                    ->whereNull('t_pembayaranspt.t_tglverifikasiberkas')
                    ->whereNull('t_pembayaranspt.t_tglverifikasispt')
                    ->whereNull('t_pembayaranspt.t_tanggalpembayaran')
                    ->whereBetween('t_pembayaranspt.t_tglverifikasiberkas', [$tgl0, $tgl1]);
                $status = 'Diperiksa';
                break;
            case 2:
                $data = $data
                    ->whereNotNull('t_pembayaranspt.t_tglverifikasiberkas')
                    ->whereNull('t_pembayaranspt.t_tglverifikasispt')
                    ->whereNull('t_pembayaranspt.t_tanggalpembayaran')
                    ->whereBetween('t_pembayaranspt.t_tglverifikasiberkas', [$tgl0, $tgl1]);
                $status = 'Diverifikasi';
                break;
            case 3:
                $data = $data
                    ->whereNotNull('t_pembayaranspt.t_tglverifikasiberkas')
                    ->whereNotNull('t_pembayaranspt.t_tglverifikasispt')
                    ->whereBetween('t_pembayaranspt.t_tglverifikasispt', [$tgl0, $tgl1])
                    ->whereNull('t_pembayaranspt.t_tanggalpembayaran');
                $status = 'Divalidasi';
                break;
            case 4:
                $data = $data
                    ->whereNotNull('t_pembayaranspt.t_tglverifikasiberkas')
                    ->whereNotNull('t_pembayaranspt.t_tglverifikasispt')
                    ->whereNotNull('t_pembayaranspt.t_tanggalpembayaran')
                    ->whereBetween('t_pembayaranspt.t_tanggalpembayaran', [$tgl0, $tgl1]);
                $status = 'Selesai';
                break;
            default:
                abort(response()->json(Message::dataSptNotFound($request['status']), 200));
        }
        $data = $data->get();
        $res = [];
        if ($data->count() == 0) {
            abort(response()->json(Message::dataSptNotFound($request['status']), 200));
        }

        foreach ($data as $key => $val) {
            if ($request['status'] == 1) {
                $tgl = $val->t_tglprosesspt;
            } elseif ($request['status'] == 2) {
                $tgl = $val->t_tglverifikasiberkas;
            } elseif ($request['status'] == 3) {
                $tgl = $val->t_tglverifikasispt;
            } elseif ($request['status'] == 4) {
                $tgl = $val->t_tanggalpembayaran;
            }

            $bphtb = $val->t_totalspt;
            if ($val->p_idpemeriksaan != null) {
                $bphtb = $val->p_totalspt;
                if ($val->t_totalspt < $val->p_totalspt) {
                    $bphtb = $val->p_totalspt;
                } else {
                    $bphtb = $val->t_totalspt;
                }
            }

            $spt = [
                'id_transaksi' => $val->t_kohirspt,
                'nop' => str_replace('.', '', $val->t_nopbphtbsppt),
                'jenis_transaksi' => $val->s_namajenistransaksi,
                'bphtb' => $bphtb,
                'kode_billing' => $val->t_kodebayarbanksppt,
                'nama_wp' => $val->t_namawppembeli,
                'telp_wp' => $val->t_telponwppembeli,
                'tanggal' => $val->t_tglprosesspt,
                'tanggal_diperbarui' => $tgl,
                'status' => $status,
                'jatuh_tempo' => null,
                'notaris' => $val->s_namanotaris,
            ];
            $res[] = $spt;
        }

        usort($res, function ($a, $b) {
            return strtotime($b['tanggal_diperbarui']) - strtotime($a['tanggal_diperbarui']);
        });
        // } else {
        //     $res = "data tidak ditemukan";
        //     return response()->json(Message::dataSptNotFound($res), 200);
        // }

        return $res;
    }

    public function getPermohonanByBilling($request)
    {
        $data = SptBphtbModel::select(
            't_spt.*',
            't_detailsptbphtb.t_namawppembeli',
            't_detailsptbphtb.t_telponwppembeli',
            's_jenistransaksi.s_namajenistransaksi',
            't_pembayaranspt.t_tglverifikasiberkas',
            't_pembayaranspt.t_tglverifikasispt',
            't_pembayaranspt.t_tanggalpembayaran',
            't_pembayaranspt.t_kodebayarbanksppt',
            't_pemeriksaan.p_totalspt',
            't_pemeriksaan.p_idpemeriksaan'
        )
            ->leftJoin('t_detailsptbphtb', 't_detailsptbphtb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->leftJoin('t_pembayaranspt', 't_pembayaranspt.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt', '=', 't_pembayaranspt.t_idpembayaranspt')
            ->where('t_pembayaranspt.t_kodebayarbanksppt', '=', $request['billing']);
        // varDumpHehe($data);
        // exit;
        $data = $data->first();
        $bphtb = $data->t_totalspt;
        if ($data->p_idpemeriksaan != null) {
            $bphtb = $data->p_totalspt;
            if ($data->t_totalspt < $data->p_totalspt) {
                $bphtb = $data->p_totalspt;
            } else {
                $bphtb = $data->t_totalspt;
            }
        }

        $res = [
            'id_transaksi' => $data->t_kohirspt,
            'kode_billing' => $data->t_kodebayarbanksppt,
            'nama_wp' => $data->t_namawppembeli,
            'nop' => str_replace('.', '', $data->t_nopbphtbsppt),
            'jenis_transaksi' => $data->s_namajenistransaksi,
            'bphtb' => $bphtb,
            'dibayar' => $data->t_tanggalpembayaran != null ? 'Lunas' : 'Belum',
            'tanggal' => $data->t_tglprosesspt,
            'tgl_dibayar' => $data->t_tanggalpembayaran,
            'jatuh_tempo' => null
        ];

        return $res;
    }

    public function getPermohonanByDibayar($request)
    {
        $tgl0 = date('Y-m-d', strtotime($request['datefrom']));
        $tgl1 = date('Y-m-d', strtotime($request['dateto']));

        $data = SptBphtbModel::select(
            't_spt.*',
            't_detailsptbphtb.t_namawppembeli',
            't_detailsptbphtb.t_telponwppembeli',
            's_jenistransaksi.s_namajenistransaksi',
            't_pembayaranspt.t_tglverifikasiberkas',
            't_pembayaranspt.t_tglverifikasispt',
            't_pembayaranspt.t_tanggalpembayaran',
            't_pembayaranspt.t_kodebayarbanksppt',
            's_notaris.s_namanotaris',
            't_pemeriksaan.p_totalspt',
            't_pemeriksaan.p_idpemeriksaan'
        )
            ->leftJoin('t_detailsptbphtb', 't_detailsptbphtb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->leftJoin('t_pembayaranspt', 't_pembayaranspt.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_notaris', 's_notaris.s_idnotaris', '=', 't_spt.t_idnotarisspt')
            ->leftJoin('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt', '=', 't_pembayaranspt.t_idpembayaranspt');
        if ($request['dibayar'] == 1) {
            $data = $data->whereNotNull('t_pembayaranspt.t_tanggalpembayaran')
                ->whereBetween('t_pembayaranspt.t_tanggalpembayaran', [$tgl0, $tgl1]);
        } else {
            $data = $data->whereNull('t_pembayaranspt.t_tanggalpembayaran')
                ->whereBetween('t_spt.t_tglprosesspt', [$tgl0, $tgl1]);
        }

        $data = $data->get();

        foreach ($data as $key => $val) {
            $bphtb = $val->t_totalspt;
            if ($val->p_idpemeriksaan != null) {
                $bphtb = $val->p_totalspt;
                if ($val->t_totalspt < $val->p_totalspt) {
                    $bphtb = $val->p_totalspt;
                } else {
                    $bphtb = $val->t_totalspt;
                }
            }
            $spt = [
                'id_transaksi' => $val->t_kohirspt,
                'nop' => str_replace('.', '', $val->t_nopbphtbsppt),
                'jenis_transaksi' => $val->s_namajenistransaksi,
                'bphtb' => $bphtb,
                'kode_billing' => $val->t_kodebayarbanksppt,
                'nama_wp' => $val->t_namawppembeli,
                'telp_wp' => $val->t_telponwppembeli,
                'tanggal' => $val->t_tglprosesspt,
                'tanggal_diperbarui' => $request['dibayar'] == 1 ? date('Y-m-d', strtotime($val->t_tanggalpembayaran)) : date('Y-m-d', strtotime($val->t_tglprosesspt)),
                'diupdate' => $val->t_tanggalpembayaran != null ? $val->t_tanggalpembayaran : $val->t_tglprosesspt,
                'dibayar' => $val->t_tanggalpembayaran != null ? 'Lunas' : 'Belum',
                'tgl_bayar' => $val->t_tanggalpembayaran != null ? $val->t_tanggalpembayaran : null,
                'jatuh_tempo' => null,
                'notaris' => $val->s_namanotaris,
            ];
            $res[] = $spt;
        }

        usort($res, function ($a, $b) {
            return strtotime($b['tanggal_diperbarui']) - strtotime($a['tanggal_diperbarui']);
        });

        return $res;
    }
}
