<?php

namespace App\Http\Traits\Pbb;

use App\ErrorMessage\Message;
use App\Helper\ApiBankHelper;
use App\Models\datapbb\DatObjekPajak;
use App\Models\datapbb\HistObjekPajak;
use App\Models\datapbb\PembayaranSppt;
use App\Models\datapbb\PstPelayanan;
use App\Models\datapbb\RefKecamatan;
use App\Models\datapbb\RefKelurahan;
use App\Models\datapbb\Sppt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Async\Pool;

trait TagihanPbbTrait
{
    public function tagihan($request)
    {
        $nop  = ApiBankHelper::explodeNop($request['nop']);

        $tagihan = Sppt::select(
            'SPPT.*',
            'REF_DATI2.NM_DATI2',
            'REF_PROPINSI.NM_PROPINSI',
            'DAT_SUBJEK_PAJAK.NM_WP',
            'DAT_OBJEK_PAJAK.JALAN_OP',
            'DAT_OBJEK_PAJAK.RW_OP',
            'DAT_OBJEK_PAJAK.RT_OP',
            'REF_KECAMATAN.NM_KECAMATAN AS OP_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN AS OP_KELURAHAN',
            'DAT_OP_BANGUNAN.KD_JPB',
            'DAT_OP_BANGUNAN.NO_BNG',
            'DAT_OP_BANGUNAN.THN_DIBANGUN_BNG',
            'DAT_OP_BANGUNAN.THN_RENOVASI_BNG',
            'DAT_OP_BANGUNAN.JML_LANTAI_BNG',
            'DAT_OP_BANGUNAN.LUAS_BNG',
            'DAT_OP_BANGUNAN.KONDISI_BNG',
            'DAT_OP_BANGUNAN.JNS_KONSTRUKSI_BNG',
            'DAT_OP_BANGUNAN.KD_DINDING',
            'DAT_OP_BANGUNAN.KD_LANTAI',
            'DAT_OP_BANGUNAN.KD_LANGIT_LANGIT',
            'DAT_OP_BUMI.KD_ZNT',
            'DAT_OP_BUMI.JNS_BUMI',
            'DAT_SUBJEK_PAJAK.TELP_WP',
        )
            // DB::raw(('HITUNG_DENDA(SPPT.TGL_JATUH_TEMPO_SPPT,SPPT.PBB_YG_HARUS_DIBAYAR_SPPT) as denda'))
            // ->selectRaw("HIT_DENDA(PBB_YG_HARUS_DIBAYAR_SPPT,TO_DATE(SPPT.TGL_JATUH_TEMPO_SPPT, 'dd-mm-yyyy'), sysdate)")
            ->selectRaw('(select HIT_DENDA ( SPPT.PBB_YG_HARUS_DIBAYAR_SPPT, SPPT.TGL_JATUH_TEMPO_SPPT, SYSDATE ) from dual) as denda')
            // ->selectRaw('(select HITUNG_DENDA ( SPPT.TGL_JATUH_TEMPO_SPPT, SPPT.PBB_YG_HARUS_DIBAYAR_SPPT ) from dual) as denda')
            ->leftJoin('REF_DATI2', [
                ['REF_DATI2.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_DATI2.KD_DATI2', '=', 'SPPT.KD_DATI2']
            ])
            ->leftJoin('REF_PROPINSI', [
                ['REF_PROPINSI.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
            ])
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP']
            ])
            ->leftJoin('DAT_OP_BANGUNAN', [
                ['DAT_OP_BANGUNAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OP_BANGUNAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OP_BANGUNAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OP_BANGUNAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OP_BANGUNAN.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OP_BANGUNAN.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OP_BANGUNAN.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP']
            ])
            ->leftJoin('DAT_OP_BUMI', [
                ['DAT_OP_BUMI.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OP_BUMI.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OP_BUMI.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OP_BUMI.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OP_BUMI.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OP_BUMI.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OP_BUMI.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP']
            ])
            ->leftJoin('DAT_SUBJEK_PAJAK', [
                ['DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID']
            ])
            ->where([
                ['SPPT.KD_PROPINSI', '=', $nop['KD_PROPINSI']],
                ['SPPT.KD_DATI2', '=', $nop['KD_DATI2']],
                ['SPPT.KD_KECAMATAN', '=', $nop['KD_KECAMATAN']],
                ['SPPT.KD_KELURAHAN', '=', $nop['KD_KELURAHAN']],
                ['SPPT.KD_BLOK', '=', $nop['KD_BLOK']],
                ['SPPT.NO_URUT', '=', $nop['NO_URUT']],
                ['SPPT.KD_JNS_OP', '=', $nop['KD_JNS_OP']],
                ['SPPT.KD_JNS_OP', '=', $nop['KD_JNS_OP']]
            ]);

        if ($request['jumlahtahun'] > 0) {
            $tagihan = $tagihan->limit($request['jumlahtahun']);
        }
        // varDumpHehe($tagihan);

        $tagihan = $tagihan->orderBy('THN_PAJAK_SPPT', 'DESC')->get();
        // $tagihan = $tagihan->get();

        foreach ($tagihan as $key => $val) {
            // $denda = (new Sppt())->hitungDenda();
            $dendaPembayaran = $val->denda;
            if ($val->status_pembayaran_sppt == 1) {
                $dendaPembayaran = PembayaranSppt::where([
                    ['KD_PROPINSI', '=', $val->kd_propinsi],
                    ['KD_DATI2', '=', $val->kd_dati2],
                    ['KD_KECAMATAN', '=', $val->kd_kecamatan],
                    ['KD_KELURAHAN', '=', $val->kd_kelurahan],
                    ['KD_BLOK', '=', $val->kd_blok],
                    ['NO_URUT', '=', $val->no_urut],
                    ['KD_JNS_OP', '=', $val->kd_jns_op],
                    ['KD_JNS_OP', '=', $val->kd_jns_op],
                    ['THN_PAJAK_SPPT', '=', $val->thn_pajak_sppt],
                ])->first();
                // dd($dendaPembayaran);
                $dendaPembayaran = $dendaPembayaran->denda_sppt;
                // $tglPembayaran = $dendaPembayaran->TGL_PEMBAYARAN_SPPT;
            }
            $data = [
                'nop' => $nop['NOP'],
                'alamat' => $val->jln_wp_sppt,
                'alamat_op' => $val->jalan_op,
                'rt_op' => $val->rt_op,
                'rw_op' => $val->rw_op,
                'blok_op' => $val->kd_blok,
                'kelurahan' => $val->op_kelurahan,
                'kecamatan' => $val->op_kecamatan,
                'kabupaten' => $val->nm_dati2,
                'provinsi' => $val->nm_propinsi,
                'luas_bumi_sppt' => $val->luas_bumi_sppt,
                'njop_bumi_sppt' => $val->njop_bumi_sppt,
                'luas_bng_sppt' => $val->luas_bng_sppt,
                'njop_bng_sppt' => $val->njop_bng_sppt,
                'jns_bumi' => $val->jns_bumi,
                'no_bng' => $val->no_bng,
                'kd_jpb' => $val->kd_jpb,
                'thn_dibangun_bng' => $val->thn_dibangun_bng,
                'thn_renovasi_bng' => $val->thn_renovasi_bng,
                'jml_lantai_bng' => $val->jml_lantai_bng,
                'luas_bng' => $val->luas_bng,
                'kondisi_bng' => $val->kondisi_bng,
                'jns_konstruksi_bng' => $val->jns_konstruksi_bng,
                'kd_dinding' => $val->kd_dinding,
                'kd_lantai' => $val->kd_lantai,
                'kd_langit_langit' => $val->kd_langit_langit,
                'tahun' => $val->thn_pajak_sppt,
                'kd_znt' => $val->kd_znt,
                'nama' => $val->nm_wp_sppt,
                'namas' => $val->nm_wp_sppt,
                'nominal_pokok' => $val->pbb_yg_harus_dibayar_sppt,
                'denda' => $dendaPembayaran,
                'status' => $val->status_pembayaran_sppt == 1 ? 'LUNAS'  : 'BELUM',
                'telp_wp' => $val->telp_wp
            ];
            $arr[] = $data;
        }
        return $arr;
    }

    public function pelayananPst($nopermohonan)
    {
        $nomor = ApiBankHelper::explodeNopermohonan($nopermohonan);
        $pst = PstPelayanan::leftJoin('PST_DETAIL', [
            ['PST_DETAIL.KD_KANWIL', '=', 'PST_PERMOHONAN.KD_KANWIL'],
            ['PST_DETAIL.KD_KPPBB', '=', 'PST_PERMOHONAN.KD_KPPBB'],
            ['PST_DETAIL.THN_PELAYANAN', '=', 'PST_PERMOHONAN.THN_PELAYANAN'],
            ['PST_DETAIL.BUNDEL_PELAYANAN', '=', 'PST_PERMOHONAN.BUNDEL_PELAYANAN'],
            ['PST_DETAIL.NO_URUT_PELAYANAN', '=', 'PST_PERMOHONAN.NO_URUT_PELAYANAN']
        ])->leftJoin('PST_DATA_OP_BARU', [
            ['PST_DATA_OP_BARU.KD_KANWIL', '=', 'PST_PERMOHONAN.KD_KANWIL'],
            ['PST_DATA_OP_BARU.KD_KPPBB', '=', 'PST_PERMOHONAN.KD_KPPBB'],
            ['PST_DATA_OP_BARU.THN_PELAYANAN', '=', 'PST_PERMOHONAN.THN_PELAYANAN'],
            ['PST_DATA_OP_BARU.BUNDEL_PELAYANAN', '=', 'PST_PERMOHONAN.BUNDEL_PELAYANAN'],
            ['PST_DATA_OP_BARU.NO_URUT_PELAYANAN', '=', 'PST_PERMOHONAN.NO_URUT_PELAYANAN']
        ])->leftjoin('REF_JNS_PELAYANAN', 'REF_JNS_PELAYANAN.KD_JNS_PELAYANAN', 'PST_DETAIL.KD_JNS_PELAYANAN')
            ->where([
                ['PST_PERMOHONAN.THN_PELAYANAN', '=', $nomor['THN_PELAYANAN']],
                ['PST_PERMOHONAN.BUNDEL_PELAYANAN', '=', $nomor['BUNDEL_PELAYANAN']],
                ['PST_PERMOHONAN.NO_URUT_PELAYANAN', '=', $nomor['NO_URUT_PELAYANAN']],
            ])->select(
                'PST_DETAIL.THN_PELAYANAN as THN_PELAYANAN_DETAIL',
                'PST_DETAIL.BUNDEL_PELAYANAN as BUNDEL_PELAYANAN_DETAIL',
                'PST_DETAIL.NO_URUT_PELAYANAN as NO_URUT_PELAYANAN_DETAIL',
                'PST_DETAIL.KD_PROPINSI_PEMOHON as KD_PROPINSI_PEMOHON_DETAIL',
                'PST_DETAIL.KD_DATI2_PEMOHON as KD_DATI2_PEMOHON_DETAIL',
                'PST_DETAIL.KD_KECAMATAN_PEMOHON as KD_KECAMATAN_PEMOHON_DETAIL',
                'PST_DETAIL.KD_KELURAHAN_PEMOHON as KD_KELURAHAN_PEMOHON_DETAIL',
                'PST_DETAIL.KD_BLOK_PEMOHON as KD_BLOK_PEMOHON_DETAIL',
                'PST_DETAIL.NO_URUT_PEMOHON as NO_URUT_PEMOHON_DETAIL',
                'PST_DETAIL.KD_JNS_OP_PEMOHON as KD_JNS_OP_PEMOHON_DETAIL',
                'PST_DETAIL.KD_JNS_PELAYANAN as KD_JNS_PELAYANAN_DETAIL',
                'PST_DETAIL.THN_PAJAK_PERMOHONAN as THN_PAJAK_PERMOHONAN_DETAIL',
                'PST_DETAIL.NAMA_PENERIMA as NAMA_PENERIMA_DETAIL',
                'PST_DETAIL.CATATAN_PENYERAHAN as CATATAN_PENYERAHAN_DETAIL',
                'PST_DETAIL.STATUS_SELESAI as STATUS_SELESAI_DETAIL',
                'PST_DETAIL.TGL_SELESAI as TGL_SELESAI_DETAIL',
                'PST_DETAIL.KD_SEKSI_BERKAS as KD_SEKSI_BERKAS_DETAIL',
                'PST_DETAIL.TGL_PENYERAHAN as TGL_PENYERAHAN_DETAIL',
                'PST_DETAIL.NIP_PENYERAH as NIP_PENYERAH_DETAIL',
                'PST_DATA_OP_BARU.KD_PROPINSI_PEMOHON as KD_PROPINSI_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_DATI2_PEMOHON as KD_DATI2_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_KECAMATAN_PEMOHON as KD_KECAMATAN_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_KELURAHAN_PEMOHON as KD_KELURAHAN_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_BLOK_PEMOHON as KD_BLOK_PEMOHON_BARU',
                'PST_DATA_OP_BARU.NO_URUT_PEMOHON as NO_URUT_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_JNS_OP_PEMOHON as KD_JNS_OP_PEMOHON_BARU',
                'PST_DATA_OP_BARU.NAMA_WP_BARU as NAMA_WP_BARU_BARU',
                'PST_DATA_OP_BARU.LETAK_OP_BARU as LETAK_OP_BARU_BARU',
                'PST_PERMOHONAN.NAMA_PEMOHON',
                'PST_PERMOHONAN.ALAMAT_PEMOHON',
                'REF_JNS_PELAYANAN.NM_JENIS_PELAYANAN',
                'PST_PERMOHONAN.THN_PELAYANAN',
                'PST_PERMOHONAN.BUNDEL_PELAYANAN',
                'PST_PERMOHONAN.NO_URUT_PELAYANAN',



            )->first();

        if ($pst->status_selesai_detail == 0) {
            $status = 'SEDANG DIPROSES';
        } elseif ($pst->status_selesai_detail == 1) {
            $status = 'SIAP KE WP';
        } elseif ($pst->status_selesai_detail == 2) {
            $status = 'SELESAI';
        }

        $res = [
            'no_permohonan' => $pst->thn_pelayanan . $pst->bundel_pelayanan . $pst->no_urut_pelayanan,
            'id_detail_permohonan' => $pst->thn_pelayanan_detail . $pst->bundel_pelayanan_detail . $pst->no_urut_pelayanan_detail . $pst->no_urut_pemohon_detail,
            'jenis_permohonan' => $pst->nm_jenis_pelayanan,
            'jenis_nop' => $pst->kd_propinsi_pemohon_baru == null ? 'INDUK' : 'PECAHAN',
            'nop_lama' => $pst->kd_propinsi_pemohon_detail . $pst->kd_dati2_pemohon_detail . $pst->kd_kecamatan_pemohon_detail . $pst->kd_kelurahan_pemohon_detail . $pst->kd_blok_pemohon_detail . $pst->kd_jns_op_pemohon_detail,
            'nop_baru' => $pst->kd_propinsi_pemohon_baru . $pst->kd_dati2_pemohon_baru . $pst->kd_kecamatan_pemohon_baru . $pst->kd_kelurahan_pemohon_baru . $pst->kd_blok_pemohon_baru . $pst->kd_jns_op_pemohon_baru,
            'nama_wp_baru' => $pst->nama_wp_baru,
            'nama_wp_lama' => $pst->nama_pemohon,
            'created_at' => $pst->tgl_selesai_detail,
            'updated_at' => $pst->tgl_selesai_detail,
            'no_hp' => 0,
            'status' => $status,
        ];

        return $res;
    }

    public function pelayananPstbyStatus($request)
    {
        $tgl0 = date('Y-m-d', strtotime($request['datefrom']));
        $tgl1 = date('Y-m-d', strtotime($request['dateto']));

        $pst = PstPelayanan::leftJoin('PST_DETAIL', [
            ['PST_DETAIL.KD_KANWIL', '=', 'PST_PERMOHONAN.KD_KANWIL'],
            ['PST_DETAIL.KD_KPPBB', '=', 'PST_PERMOHONAN.KD_KPPBB'],
            ['PST_DETAIL.THN_PELAYANAN', '=', 'PST_PERMOHONAN.THN_PELAYANAN'],
            ['PST_DETAIL.BUNDEL_PELAYANAN', '=', 'PST_PERMOHONAN.BUNDEL_PELAYANAN'],
            ['PST_DETAIL.NO_URUT_PELAYANAN', '=', 'PST_PERMOHONAN.NO_URUT_PELAYANAN']
        ])->leftJoin('PST_DATA_OP_BARU', [
            ['PST_DATA_OP_BARU.KD_KANWIL', '=', 'PST_PERMOHONAN.KD_KANWIL'],
            ['PST_DATA_OP_BARU.KD_KPPBB', '=', 'PST_PERMOHONAN.KD_KPPBB'],
            ['PST_DATA_OP_BARU.THN_PELAYANAN', '=', 'PST_PERMOHONAN.THN_PELAYANAN'],
            ['PST_DATA_OP_BARU.BUNDEL_PELAYANAN', '=', 'PST_PERMOHONAN.BUNDEL_PELAYANAN'],
            ['PST_DATA_OP_BARU.NO_URUT_PELAYANAN', '=', 'PST_PERMOHONAN.NO_URUT_PELAYANAN']
        ])->leftjoin('REF_JNS_PELAYANAN', 'REF_JNS_PELAYANAN.KD_JNS_PELAYANAN', 'PST_DETAIL.KD_JNS_PELAYANAN')
            // ->whereYear('PST_PERMOHONAN.TGL_TERIMA_DOKUMEN_WP', $tahun)
            // ->where('PST_PERMOHONAN.TGL_TERIMA_DOKUMEN_WP', '<=', $tanggal)
            ->whereBetween('PST_PERMOHONAN.TGL_TERIMA_DOKUMEN_WP', [$tgl0, $tgl1])
            ->where('PST_DETAIL.STATUS_SELESAI', $request['status'])
            ->select(
                'PST_DETAIL.THN_PELAYANAN as THN_PELAYANAN_DETAIL',
                'PST_DETAIL.BUNDEL_PELAYANAN as BUNDEL_PELAYANAN_DETAIL',
                'PST_DETAIL.NO_URUT_PELAYANAN as NO_URUT_PELAYANAN_DETAIL',
                'PST_DETAIL.KD_PROPINSI_PEMOHON as KD_PROPINSI_PEMOHON_DETAIL',
                'PST_DETAIL.KD_DATI2_PEMOHON as KD_DATI2_PEMOHON_DETAIL',
                'PST_DETAIL.KD_KECAMATAN_PEMOHON as KD_KECAMATAN_PEMOHON_DETAIL',
                'PST_DETAIL.KD_KELURAHAN_PEMOHON as KD_KELURAHAN_PEMOHON_DETAIL',
                'PST_DETAIL.KD_BLOK_PEMOHON as KD_BLOK_PEMOHON_DETAIL',
                'PST_DETAIL.NO_URUT_PEMOHON as NO_URUT_PEMOHON_DETAIL',
                'PST_DETAIL.KD_JNS_OP_PEMOHON as KD_JNS_OP_PEMOHON_DETAIL',
                'PST_DETAIL.KD_JNS_PELAYANAN as KD_JNS_PELAYANAN_DETAIL',
                'PST_DETAIL.THN_PAJAK_PERMOHONAN as THN_PAJAK_PERMOHONAN_DETAIL',
                'PST_DETAIL.NAMA_PENERIMA as NAMA_PENERIMA_DETAIL',
                'PST_DETAIL.CATATAN_PENYERAHAN as CATATAN_PENYERAHAN_DETAIL',
                'PST_DETAIL.STATUS_SELESAI as STATUS_SELESAI_DETAIL',
                'PST_DETAIL.TGL_SELESAI as TGL_SELESAI_DETAIL',
                'PST_DETAIL.KD_SEKSI_BERKAS as KD_SEKSI_BERKAS_DETAIL',
                'PST_DETAIL.TGL_PENYERAHAN as TGL_PENYERAHAN_DETAIL',
                'PST_DETAIL.NIP_PENYERAH as NIP_PENYERAH_DETAIL',
                'PST_DATA_OP_BARU.KD_PROPINSI_PEMOHON as KD_PROPINSI_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_DATI2_PEMOHON as KD_DATI2_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_KECAMATAN_PEMOHON as KD_KECAMATAN_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_KELURAHAN_PEMOHON as KD_KELURAHAN_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_BLOK_PEMOHON as KD_BLOK_PEMOHON_BARU',
                'PST_DATA_OP_BARU.NO_URUT_PEMOHON as NO_URUT_PEMOHON_BARU',
                'PST_DATA_OP_BARU.KD_JNS_OP_PEMOHON as KD_JNS_OP_PEMOHON_BARU',
                'PST_DATA_OP_BARU.NAMA_WP_BARU as NAMA_WP_BARU_BARU',
                'PST_DATA_OP_BARU.LETAK_OP_BARU as LETAK_OP_BARU_BARU',
                'PST_PERMOHONAN.NAMA_PEMOHON',
                'PST_PERMOHONAN.ALAMAT_PEMOHON',
                'REF_JNS_PELAYANAN.NM_JENIS_PELAYANAN',
                'PST_PERMOHONAN.THN_PELAYANAN',
                'PST_PERMOHONAN.BUNDEL_PELAYANAN',
                'PST_PERMOHONAN.NO_URUT_PELAYANAN',
            )->get();

        foreach ($pst as $key => $val) {
            if ($val->status_selesai_detail == 0) {
                $status = 'SEDANG DIPROSES';
            } elseif ($val->status_selesai_detail == 1) {
                $status = 'SIAP KE WP';
            } elseif ($val->status_selesai_detail == 2) {
                $status = 'SELESAI';
            }
            $data = [
                'no_permohonan' => $val->thn_pelayanan . $val->bundel_pelayanan . $val->no_urut_pelayanan,
                'id_detail_permohonan' => $val->thn_pelayanan_detail . $val->bundel_pelayanan_detail . $val->no_urut_pelayanan_detail . $val->no_urut_pemohon_detail,
                'jenis_permohonan' => $val->nm_jenis_pelayanan,
                'jenis_nop' => $val->kd_propinsi_pemohon_baru == null ? 'INDUK' : 'PECAHAN',
                'nop_lama' => $val->kd_propinsi_pemohon_detail . $val->kd_dati2_pemohon_detail . $val->kd_kecamatan_pemohon_detail . $val->kd_kelurahan_pemohon_detail . $val->kd_blok_pemohon_detail . $val->kd_jns_op_pemohon_detail,
                'nop_baru' => $val->kd_propinsi_pemohon_baru . $val->kd_dati2_pemohon_baru . $val->kd_kecamatan_pemohon_baru . $val->kd_kelurahan_pemohon_baru . $val->kd_blok_pemohon_baru . $val->kd_jns_op_pemohon_baru,
                'nama_wp_baru' => $val->nama_wp_baru,
                'nama_wp_lama' => $val->nama_pemohon,
                'created_at' => $val->tgl_selesai_detail,
                'updated_at' => $val->tgl_selesai_detail,
                'no_hp' => 0,
                'status' => $status,
            ];
            $res[] = $data;
        }



        return $res;
    }

    // public function tagihankeckel($request)
    // {
    //     $tagihan = Sppt::select(
    //         'SPPT.*',
    //         'DAT_SUBJEK_PAJAK.NM_WP',
    //         'DAT_OBJEK_PAJAK.JALAN_OP',
    //         'DAT_OBJEK_PAJAK.RW_OP',
    //         'DAT_OBJEK_PAJAK.RT_OP',
    //         'REF_KECAMATAN.NM_KECAMATAN AS OP_KECAMATAN',
    //         'REF_KELURAHAN.NM_KELURAHAN AS OP_KELURAHAN',
    //     )
    //         ->leftJoin('REF_KECAMATAN', [
    //             ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
    //             ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
    //             ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
    //         ])
    //         ->leftJoin('REF_KELURAHAN', [
    //             ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
    //             ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
    //             ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
    //             ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
    //         ])
    //         ->leftJoin('DAT_OBJEK_PAJAK', [
    //             ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
    //             ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
    //             ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
    //             ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
    //             ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'SPPT.KD_BLOK'],
    //             ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'SPPT.NO_URUT'],
    //             ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP']
    //         ])
    //         ->leftJoin('DAT_SUBJEK_PAJAK', [
    //             ['DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID']
    //         ])
    //         ->where('SPPT.THN_PAJAK_SPPT', '=', $request['tahun']);
    //     $kec = strtoupper($request['kecamatan']);
    //     $kodekec = RefKecamatan::where('NM_KECAMATAN', $kec)->first();
    //     $tagihan = $tagihan->where('SPPT.KD_KECAMATAN', $kodekec->kd_kecamatan);
    //     if ($request['kelurahan'] != null) {
    //         $kodekel = RefKelurahan::where('KD_KECAMATAN', $kodekec->kd_kecamatan)->where('NM_KELURAHAN', $request['kelurahan'])->first();
    //         $tagihan = $tagihan->where('SPPT.KD_KELURAHAN', $kodekel->kd_kelurahan);
    //     }
    //     if ($request['status'] != null) {
    //         $tagihan = $tagihan->where('SPPT.STATUS_PEMBAYARAN_SPPT', $request['status']);
    //     }

    //     $tagihan = $tagihan->orderBy('THN_PAJAK_SPPT', 'DESC')->get();

    //     foreach ($tagihan as $key => $val) {
    //         $data = [
    //             'nop' =>
    // $val->kd_propinsi .
    //     $val->kd_dati2 .
    //     $val->kd_kecamatan .
    //     $val->kd_kelurahan .
    //     $val->kd_blok .
    //     $val->no_urut .
    //     $val->kd_jns_op,
    //             'alamat' => $val->jln_wp_sppt,
    //             'alamat_op' => $val->jalan_op,
    //             'rt_op' => $val->rt_op,
    //             'rw_op' => $val->rw_op,
    //             'kelurahan' => $val->op_kelurahan,
    //             'kecamatan' => $val->op_kecamatan,
    //             'nama' => $val->nm_wp_sppt,
    //             'namas' => $val->nm_wp_sppt,
    //             'nominal_pokok' => $val->pbb_terhutang_sppt,
    //             'status' => $val->status_pembayaran_sppt == 1 ? 'LUNAS' : 'BELUM'
    //         ];
    //         $arr[] = $data;
    //     }
    //     return $arr;
    // }

    // public function tagihankeckel($request)
    // {
    //     ini_set('max_execution_time', 999999);
    //     $tagihan = Sppt::where('SPPT.THN_PAJAK_SPPT', '=', $request['tahun']);
    //     $kec = strtoupper($request['kecamatan']);
    //     $kodekec = RefKecamatan::where('NM_KECAMATAN', $kec)->first();
    //     $tagihan = $tagihan->where('SPPT.KD_KECAMATAN', $kodekec->kd_kecamatan);
    //     if ($request['kelurahan'] != null) {
    //         $kodekel = RefKelurahan::where('KD_KECAMATAN', $kodekec->kd_kecamatan)->where('NM_KELURAHAN', $request['kelurahan'])->first();
    //         $tagihan = $tagihan->where('SPPT.KD_KELURAHAN', $kodekel->kd_kelurahan);
    //     }
    //     if ($request['status'] != null) {
    //         $tagihan = $tagihan->where('SPPT.STATUS_PEMBAYARAN_SPPT', $request['status']);
    //     }

    //     $tagihan = $tagihan->orderBy('THN_PAJAK_SPPT', 'DESC')
    //         ->chunk(200, function ($tagihan) use (&$arr) {
    //             foreach ($tagihan as $val) {
    //                 $data = [
    //                     'nop' => $val->kd_propinsi .
    //                         $val->kd_dati2 .
    //                         $val->kd_kecamatan .
    //                         $val->kd_kelurahan .
    //                         $val->kd_blok .
    //                         $val->no_urut .
    //                         $val->kd_jns_op,
    //                     'alamat' => $val->jln_wp_sppt,
    //                     'nama' => $val->nm_wp_sppt,
    //                     'namas' => $val->nm_wp_sppt,
    //                     'nominal_pokok' => $val->pbb_terhutang_sppt,
    //                     'status' => $val->status_pembayaran_sppt == 1 ? 'LUNAS' : 'BELUM',
    //                 ];
    //                 $arr[] = $data;
    //             }
    //         });

    //     return $arr;
    // ->get();

    // return $tagihan->map(function ($val) {
    //     return [
    // 'nop' => $val->kd_propinsi .
    //     $val->kd_dati2 .
    //     $val->kd_kecamatan .
    //     $val->kd_kelurahan .
    //     $val->kd_blok .
    //     $val->no_urut .
    //     $val->kd_jns_op,
    //         'alamat' => $val->jln_wp_sppt,
    //         // 'alamat_op' => $val->jalan_op,
    //         // 'rt_op' => $val->rt_op,
    //         // 'rw_op' => $val->rw_op,
    //         // 'kelurahan' => $val->op_kelurahan,
    //         // 'kecamatan' => $val->op_kecamatan,
    //         'nama' => $val->nm_wp_sppt,
    //         'namas' => $val->nm_wp_sppt,
    //         'nominal_pokok' => $val->pbb_terhutang_sppt,
    //         'status' => $val->status_pembayaran_sppt == 1 ? 'LUNAS' : 'BELUM',
    //     ];
    // })->toArray();
    // }

    public function tagihankeckel($request)
    {
        ini_set('max_execution_time', 999999);

        $host = $_SERVER['HTTP_HOST'];
        if (strpos($host, 'localhost') !== false) {
            $username = "BLORA";
            $password = "BLORA";
            $database = "192.168.56.101/SIMPBB";
        } else {
            $username = "PBB";
            $password = "PBB";
            $database = "192.168.0.100/SISMIOP";
        }

        $koneksi = oci_connect($username, $password, $database);
        if ($koneksi) {
            $sql = "SELECT * FROM SPPT WHERE KD_KECAMATAN = '" . $request['kecamatan'] . "' and KD_KELURAHAN ='" . $request['kelurahan'] . "' and STATUS_PEMBAYARAN_SPPT='" . $request['status'] . "' and THN_PAJAK_SPPT='" . $request['tahun'] . "' and KD_BLOK = '" . $request['kdblok'] . "'";
            // die($sql);
            $stmt = oci_parse($koneksi, $sql);
            oci_execute($stmt);
            $data = array();
            while ($res = oci_fetch_array($stmt)) {
                $data[] = $res;
            }
        }
        // dd($data);
        foreach ($data as $key => $value) {
            // dd($value);
            $hitdd = "select HIT_DENDA ( '" . $value['PBB_YG_HARUS_DIBAYAR_SPPT'] . "', TO_DATE('" . $value['TGL_JATUH_TEMPO_SPPT'] . "'), SYSDATE ) from dual";
            $stmtHitdd = oci_parse($koneksi, $hitdd);
            oci_execute($stmtHitdd);
            $result = oci_fetch_array($stmtHitdd);

            if ($value['STATUS_PEMBAYARAN_SPPT'] == 1) {
                $hitdd = "SELECT DENDA_SPPT FROM PEMBAYARAN_SPPT WHERE KD_PROPINSI || KD_DATI2|| KD_KECAMATAN || KD_KELURAHAN|| KD_BLOK|| NO_URUT || KD_JNS_OP || THN_PAJAK_SPPT = '" . $value['KD_PROPINSI'] . $value['KD_DATI2'] . $value['KD_KECAMATAN'] .
                    $value['KD_KELURAHAN'] . $value['KD_BLOK'] . $value['NO_URUT'] . $value['KD_JNS_OP'] . $value['THN_PAJAK_SPPT'] . "'";
                // die($hitdd);
                $stmtHitdd = oci_parse($koneksi, $hitdd);
                oci_execute($stmtHitdd);
                $result = oci_fetch_array($stmtHitdd);
            }
            $ajg = [
                'nop' => $value['KD_PROPINSI'] . $value['KD_DATI2'] . $value['KD_KECAMATAN'] .
                    $value['KD_KELURAHAN'] . $value['KD_BLOK'] . $value['NO_URUT'] . $value['KD_JNS_OP'],
                'alamat' => $value['JLN_WP_SPPT'],
                'nama' => $value['NM_WP_SPPT'],
                'namas' => $value['NM_WP_SPPT'],
                'nominal_pokok' => $value['PBB_YG_HARUS_DIBAYAR_SPPT'],
                'denda' =>  $result[0],
                'status' => $value['STATUS_PEMBAYARAN_SPPT'] == 1 ? 'LUNAS' : 'BELUM',
            ];
            $arrData[] = $ajg;
        }
        return $arrData;
    }

    private function getKodeKecamatan($kecamatan)
    {
        return RefKecamatan::where('NM_KECAMATAN', strtoupper($kecamatan))->value('kd_kecamatan');
    }

    private function getKodeKelurahan($kelurahan)
    {
        return RefKelurahan::where('NM_KELURAHAN', $kelurahan)->value('kd_kelurahan');
    }

    public function statusByr($request)
    {
        $tgl1 = date('Y-m-d', strtotime($request['datefrom']));
        $tgl2 = date('Y-m-d', strtotime($request['dateto']));

        $data = PembayaranSppt::whereBetween('TGL_PEMBAYARAN_SPPT', [$tgl1, $tgl2])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'PEMBAYARAN_SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'PEMBAYARAN_SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'PEMBAYARAN_SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'PEMBAYARAN_SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'PEMBAYARAN_SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'PEMBAYARAN_SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'PEMBAYARAN_SPPT.KD_JNS_OP']
            ])
            ->leftJoin('DAT_SUBJEK_PAJAK', [
                ['DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID']
            ])
            ->where('THN_PAJAK_SPPT', $request['thnpajak'])
            ->orderBy('tgl_rekam_byr_sppt', 'desc')
            ->get();

        $arrData = [];
        foreach ($data as $key => $value) {
            $ajg = [
                'nop' => $value['kd_propinsi'] . $value['kd_dati2'] . $value['kd_kecamatan'] .
                    $value['kd_kelurahan'] . $value['kd_blok'] . $value['no_urut'] . $value['kd_jns_op'],
                'thnpajak' => $value['thn_pajak_sppt'],
                'nm_wp' => $value['nm_wp'],
                'alamat_op' => $value['jalan_op'],
                'rt_op' => $value['rt_op'],
                'rw_op' => $value['rw_op'],
                'telp_wp' => $value['telp_wp'],
                'tgl_rekam_byr_sppt' => $value['tgl_rekam_byr_sppt'],
                'jml_sppt_yg_dibayar' => $value['jml_sppt_yg_dibayar'],
                'denda_sppt' => $value['denda_sppt'],
            ];
            $arrData[] = $ajg;
        }

        return $arrData;
    }

    public function pembayaranPertama($request)
    {
        $data = PembayaranSppt::where('THN_PAJAK_SPPT', $request['thnpajak'])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'PEMBAYARAN_SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'PEMBAYARAN_SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'PEMBAYARAN_SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'PEMBAYARAN_SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'PEMBAYARAN_SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'PEMBAYARAN_SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'PEMBAYARAN_SPPT.KD_JNS_OP']
            ])
            ->leftJoin('DAT_SUBJEK_PAJAK', [
                ['DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID']
            ])
            ->where('THN_PAJAK_SPPT', $request['thnpajak'])
            ->orderBy('tgl_rekam_byr_sppt', 'asc')
            ->limit(10)
            ->get();

        $arrData = [];
        foreach ($data as $key => $value) {
            $ajg = [
                'nop' => $value['kd_propinsi'] . $value['kd_dati2'] . $value['kd_kecamatan'] .
                    $value['kd_kelurahan'] . $value['kd_blok'] . $value['no_urut'] . $value['kd_jns_op'],
                'thnpajak' => $value['thn_pajak_sppt'],
                'nm_wp' => $value['nm_wp'],
                'alamat_op' => $value['jalan_op'],
                'rt_op' => $value['rt_op'],
                'rw_op' => $value['rw_op'],
                'telp_wp' => $value['telp_wp'],
                'tgl_rekam_byr_sppt' => $value['tgl_rekam_byr_sppt'],
                'jml_sppt_yg_dibayar' => $value['jml_sppt_yg_dibayar'],
                'denda_sppt' => $value['denda_sppt'],
            ];
            $arrData[] = $ajg;
        }

        return $arrData;
    }
}
