<?php

namespace App\Http\Traits\Pdl;

use App\Helper\ApiBankHelper;
use App\Models\datasimpatda\TransaksiSimpatda;

trait PdlTrait
{
    public function tagihan($request)
    {
        $npwpd = ApiBankHelper::formatnpwpd($request['npwpd']);
        $data = TransaksiSimpatda::select(
            'view_wp.t_npwpd',
            'view_wp.t_nama',
            'view_wpobjek.t_namaobjek',
            'view_wpobjek.t_alamatobjek',
            'view_wpobjek.s_namakec',
            'view_wpobjek.s_namakel',
            'view_wpobjek.s_namajenis',
            't_transaksi.*'
        )->leftJoin('view_wpobjek', 'view_wpobjek.t_idobjek', '=', 't_transaksi.t_idwpobjek')
            ->leftJoin('view_wp', 'view_wpobjek.t_idwp', '=', 'view_wp.t_idwp')
            ->where('view_wp.t_npwpd', $npwpd)
            ->where('t_transaksi.t_periodepajak', $request['tahun'])
            ->get();

        foreach ($data as $key => $val) {
            $jatuhtempo = $val->t_tgljatuhtempo;
            $tglsekarang = date('Y-m-d');

            $ts1 = strtotime($jatuhtempo);
            $ts2 = strtotime($tglsekarang);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $day1 = date('d', $ts1);
            $day2 = date('d', $ts2);
            if ($day1 < $day2) {
                $tambahanbulan = 1;
            } else {
                $tambahanbulan = 0;
            }

            $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
            $persendenda = 1;
            if ($year1 < 2024) {
                $persendenda = 2;
            }
            if ($jmlbulan > 24) {
                $jmlbulan = 24;
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            } else {
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            }
            // dd($jmldenda);
            $tagihan = [
                'id_transaksi' => $val->t_nourut,
                'kode_billing' => $val->t_kodebayar,
                'nama_wp' => $val->t_nama,
                'npwpd' => $val->t_npwpd,
                'nama_objek' => $val->t_namaobjek,
                'alamat_objek' => $val->t_alamatobjek,
                'kecamatan_objek' => $val->s_namakec,
                'kelurahan_objek' => $val->s_namakel,
                'jenis_pajak' => $val->s_namajenis,
                'pajak' => $val->t_jmlhpajak,
                'denda' => $val->t_tglpembayaran != null ? ($val->t_jmlhbayardenda ?? 0) : $jmldenda,
                'dibayar' => $val->t_jmlhpembayaran != null ? 'Lunas' : 'Belum',
                'tgl_dibayar' => $val->t_tglpembayaran,
                'jatuh_tempo' => $val->t_tgljatuhtempo
            ];
            $res[] = $tagihan;
        }

        // DB::table('t_transaksi')->select(
        //     'view_wpobjek.t_npwpdwp',
        //     'view_wpobjek.t_namawp',
        //     'view_wpobjek.t_namaobjek',
        //     'view_wpobjek.t_alamatobjek',
        //     'view_wpobjek.s_namakec',
        //     'view_wpobjek.s_namakel',
        //     'view_wpobjek.s_namajenis',
        //     't_transaksi.*'
        // )->leftJoin('view_wpobjek', 'view_wpobjek.t_idobjek', '=', 't_transaksi.t_idwpobjek')->leftJoin('view_wp', 'view_wpobjek.t_idwp', '=', 'view_wp.t_idwp')->where([['view_wp.t_npwpd', 'P.2.0093459.05.14'], ['t_transaksi.t_periodepajak', '2023']])->get();
        return $res;
    }

    public function tagihanPeriode($request)
    {
        $data = TransaksiSimpatda::select(
            'view_wp.t_npwpd',
            'view_wp.t_nama',
            'view_wp.t_notelp',
            'view_wpobjek.t_namaobjek',
            'view_wpobjek.t_alamatobjek',
            'view_wpobjek.s_namakec',
            'view_wpobjek.s_namakel',
            'view_wpobjek.s_namajenis',
            't_transaksi.*'
        )->leftJoin('view_wpobjek', 'view_wpobjek.t_idobjek', '=', 't_transaksi.t_idwpobjek')
            ->leftJoin('view_wp', 'view_wpobjek.t_idwp', '=', 'view_wp.t_idwp')
            ->whereBetween('t_masaawal', [$request['datefrom'], $request['dateto']]);
        if ($request['dibayar'] == 0) {
            $data = $data->whereNull('t_tglpembayaran');
        } else if ($request['dibayar'] == 1) {
            $data = $data->whereNotNull('t_tglpembayaran');
        }

        if ($request['jenis'] != 0) {
            $data = $data->where('t_transaksi.t_jenispajak', $request['jenis']);
        }

        $data = $data->get();
        foreach ($data as $key => $val) {
            $jatuhtempo = $val->t_tgljatuhtempo;
            $tglsekarang = date('Y-m-d');

            $ts1 = strtotime($jatuhtempo);
            $ts2 = strtotime($tglsekarang);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $day1 = date('d', $ts1);
            $day2 = date('d', $ts2);
            if ($day1 < $day2) {
                $tambahanbulan = 1;
            } else {
                $tambahanbulan = 0;
            }

            $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
            $persendenda = 1;
            if ($year1 < 2024) {
                $persendenda = 2;
            }
            if ($jmlbulan > 24) {
                $jmlbulan = 24;
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            } else {
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            }
            $tagihan = [
                'id_transaksi' => $val->t_nourut,
                'kode_billing' => $val->t_kodebayar,
                'nama_wp' => $val->t_nama,
                'npwpd' => $val->t_npwpd,
                'nohp' => $val->t_notelp ?? '-',
                'nama_objek' => $val->t_namaobjek,
                'alamat_objek' => $val->t_alamatobjek,
                'kecamatan_objek' => $val->s_namakec,
                'kelurahan_objek' => $val->s_namakel,
                'jenis_pajak' => $val->s_namajenis,
                'pajak' => $val->t_jmlhpajak,
                'denda' => $val->t_tglpembayaran != null ? ($val->t_jmlhbayardenda ?? 0) : $jmldenda,
                'dibayar' => $val->t_jmlhpembayaran != null ? 'Lunas' : 'Belum',
                'tgl_dibayar' => $val->t_tglpembayaran,
                'jatuh_tempo' => $val->t_tgljatuhtempo
            ];
            $res[] = $tagihan;
        }

        return $res;
    }

    public function tagihanBytglInput($request)
    {
        $data = TransaksiSimpatda::select(
            'view_wp.t_npwpd',
            'view_wp.t_nama',
            'view_wp.t_notelp',
            'view_wpobjek.t_namaobjek',
            'view_wpobjek.t_alamatobjek',
            'view_wpobjek.s_namakec',
            'view_wpobjek.s_namakel',
            'view_wpobjek.s_namajenis',
            't_transaksi.*'
        )->leftJoin('view_wpobjek', 'view_wpobjek.t_idobjek', '=', 't_transaksi.t_idwpobjek')
            ->leftJoin('view_wp', 'view_wpobjek.t_idwp', '=', 'view_wp.t_idwp')
            ->whereBetween('t_tglpendataan', [$request['datefrom'], $request['dateto']]);

        $data = $data->whereNull('t_tglpembayaran');

        // $data = $data->whereNotNull('t_tglpembayaran');


        $data = $data->get();
        foreach ($data as $key => $val) {
            $jatuhtempo = $val->t_tgljatuhtempo;
            $tglsekarang = date('Y-m-d');

            $ts1 = strtotime($jatuhtempo);
            $ts2 = strtotime($tglsekarang);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $day1 = date('d', $ts1);
            $day2 = date('d', $ts2);
            if ($day1 < $day2) {
                $tambahanbulan = 1;
            } else {
                $tambahanbulan = 0;
            }

            $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
            $persendenda = 1;
            if ($year1 < 2024) {
                $persendenda = 2;
            }
            if ($jmlbulan > 24) {
                $jmlbulan = 24;
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            } else {
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            }
            $tagihan = [
                'id_transaksi' => $val->t_nourut,
                'kode_billing' => $val->t_kodebayar,
                'nama_wp' => $val->t_nama,
                'npwpd' => $val->t_npwpd,
                'nohp' => $val->t_notelp ?? '-',
                'nama_objek' => $val->t_namaobjek,
                'alamat_objek' => $val->t_alamatobjek,
                'kecamatan_objek' => $val->s_namakec,
                'kelurahan_objek' => $val->s_namakel,
                'jenis_pajak' => $val->s_namajenis,
                'pajak' => $val->t_jmlhpajak,
                'denda' => $val->t_tglpembayaran != null ? ($val->t_jmlhbayardenda ?? 0) : ($jmldenda <= 0 ? 0 : $jmldenda),
                'dibayar' => $val->t_jmlhpembayaran != null ? 'Lunas' : 'Belum',
                'tgl_dibayar' => $val->t_tglpembayaran,
                'jatuh_tempo' => $val->t_tgljatuhtempo
            ];
            $res[] = $tagihan;
        }

        return $res;
    }

    public function tagihanBytglBayar($request)
    {
        $timestamp1 = date('Y-m-d H:i:s', strtotime($request['datefrom'] . ' 00:00:00'));
        $timestamp2 = date('Y-m-d H:i:s', strtotime($request['dateto'] . ' 23:59:59'));

        $data = TransaksiSimpatda::select(
            'view_wp.t_npwpd',
            'view_wp.t_nama',
            'view_wp.t_notelp',
            'view_wpobjek.t_namaobjek',
            'view_wpobjek.t_alamatobjek',
            'view_wpobjek.s_namakec',
            'view_wpobjek.s_namakel',
            'view_wpobjek.s_namajenis',
            't_transaksi.*'
        )->leftJoin('view_wpobjek', 'view_wpobjek.t_idobjek', '=', 't_transaksi.t_idwpobjek')
            ->leftJoin('view_wp', 'view_wpobjek.t_idwp', '=', 'view_wp.t_idwp')
            ->whereBetween('t_tglpembayaran', [$timestamp1, $timestamp2]);

        // varDumpHehe($data);

        $data = $data->get();
        foreach ($data as $key => $val) {
            $jatuhtempo = $val->t_tgljatuhtempo;
            $tglsekarang = date('Y-m-d');

            $ts1 = strtotime($jatuhtempo);
            $ts2 = strtotime($tglsekarang);

            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);

            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $day1 = date('d', $ts1);
            $day2 = date('d', $ts2);
            if ($day1 < $day2) {
                $tambahanbulan = 1;
            } else {
                $tambahanbulan = 0;
            }

            $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
            $persendenda = 1;
            if ($year1 < 2024) {
                $persendenda = 2;
            }
            if ($jmlbulan > 24) {
                $jmlbulan = 24;
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            } else {
                $jmldenda = $jmlbulan * $persendenda / 100 * $val->t_jmlhpajak;
            }
            $tagihan = [
                'id_transaksi' => $val->t_nourut,
                'kode_billing' => $val->t_kodebayar,
                'nama_wp' => $val->t_nama,
                'npwpd' => $val->t_npwpd,
                'nohp' => $val->t_notelp ?? '-',
                'nama_objek' => $val->t_namaobjek,
                'alamat_objek' => $val->t_alamatobjek,
                'kecamatan_objek' => $val->s_namakec,
                'kelurahan_objek' => $val->s_namakel,
                'jenis_pajak' => $val->s_namajenis,
                'pajak' => $val->t_jmlhpajak,
                'denda' => $val->t_tglpembayaran != null ? ($val->t_jmlhbayardenda ?? 0) : ($jmldenda <= 0 ? 0 : $jmldenda),
                'dibayar' => $val->t_jmlhpembayaran != null ? 'Lunas' : 'Belum',
                'tgl_dibayar' => $val->t_tglpembayaran,
                'jatuh_tempo' => $val->t_tgljatuhtempo
            ];
            $res[] = $tagihan;
        }

        return $res;
    }
}
