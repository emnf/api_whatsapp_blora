<?php

namespace App\Models\databphtb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisTransaksiModel extends Model
{
    use HasFactory;

    protected $connection = 'bphtb';
    protected $table    = 's_jenistransaksi';
    protected $primaryKey = 's_idjenistransaksi';
    protected $fillable = ['s_kodejenistransaksi', 's_namajenistransaksi'];
}
