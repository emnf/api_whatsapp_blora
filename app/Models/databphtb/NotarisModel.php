<?php

namespace App\Models\databphtb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotarisModel extends Model
{
    use HasFactory;

    protected $connection = 'bphtb';
    protected $table    = 's_notaris';
    protected $primaryKey = 's_idnotaris';
    protected $fillable = ['s_namanotaris', 's_alamatnotaris', 's_kodenotaris', 's_sknotaris', 's_statusnotaris'];
}
