<?php

namespace App\Models\databphtb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PejabatBphtbModel extends Model
{
    use HasFactory;
    protected $connection = 'bphtb';
    protected $table = 's_pejabat';
    protected $primaryKey = 's_idpejabat';
}
