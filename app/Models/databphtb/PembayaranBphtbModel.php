<?php

namespace App\Models\databphtb;

use App\Models\databphtb\SptBphtbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PembayaranBphtbModel extends Model
{
  use HasFactory;

  protected $connection = 'bphtb';
  protected $table    = 't_pembayaranspt';
  protected $primaryKey = 't_idpembayaranspt';
  protected $fillable = [
    't_idspt',
    't_kohirpembayaran',
    't_periodepembayaran',
    't_tanggalpembayaran',
    't_objekspt',
    't_idnotaris',
    't_ketetapanspt',
    't_nilaipembayaranspt',
    't_idkorekspt',
    't_kodebayarspt',
    't_verifikasispt',
    't_tglverifikasispt',
    't_pejabatverifikasispt',
    't_statusbayarspt',
    't_kodebayarbanksppt',
    't_dendabulan',
    't_pejabatpembayaranspt',
    't_idds',
    't_idpenerimasetoran',
    't_verifikasiberkas',
    't_tglverifikasiberkas',
    't_kodebayarbanksppt_lama',
    't_user_update_kodebayar',
    't_tgl_update_kodebayar',
    't_ket_update_kodebayar',
    't_pejabatverifikasiberkas',
    't_ket_verifikasi_berkas',
    's_id_format_pesan',

  ];

  public function PembayaranBphtbModel()
  {
    return $this->belongsTo(SptBphtbModel::class, 't_idspt', 't_idspt')->withDefault(function ($data, $post) {
      $data->t_payment_refnum = null;
    });
  }
}
