<?php

namespace App\Models\databphtb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SptBphtbModel extends Model
{
    use HasFactory;

    protected $connection = 'bphtb';
    protected $table = 't_spt';
    protected $primaryKey = 't_idspt';
    protected $fillable = [
        't_kohirspt',
        't_kohirketetapanspt',
        't_tglprosesspt',
        't_periodespt',
        't_idnotarisspt',
        't_objekspt',
        't_idtarifspt',
        't_ketetapanspt',
        't_tglketetapanspt',
        't_tgljatuhtempospt',
        't_nopbphtbsppt',
        't_kodebayarbanksppt',
        't_idjenistransaksi',
        't_idjenishaktanah',
        't_idrefspt',
        't_pejabatverifikasispt',
        't_dasarspt',
        't_totalspt',
        't_nilaitransaksispt',
        't_potonganspt',
        't_thnsppt',
        't_persyaratan',
        't_idjenisdoktanah',
        't_idsptsebelumnya',
        't_pejabatpendaftaranspt',
        't_idtarifbphtb',
        't_input_sismiop',
        't_iduser_sismiop',
        't_tglproses_sismiop',
        't_tarif_pembagian_aphb_kali',
        't_tarif_pembagian_aphb_bagi',
        't_persenbphtb',
        'fr_tervalidasidua',
        't_potongan_waris_hibahwasiat',
        't_kodedaftarspt',
        't_jenispembelian',
        't_approve_pengurangpembebas',
        't_keterangan',
        't_latitudeobjek',
        't_longitudeobjek',
        't_persyaratan_sudah_upload',
        't_id_tipe_espop',
        't_konfirm_espop',
        't_tgl_konfirm_espop',
        't_user_konfirm_espop'

    ];

    public function dataNotaris()
    {
        return $this->hasOne(NotarisModel::class, 's_idnotaris', 't_idnotaris_spt');
    }

    public function dataJenisTransaksi()
    {
        return $this->hasOne(JenisTransaksiModel::class, 's_idjenistransaksi', 't_idjenistransaksi');
    }

    public function dataPemeriksaan()
    {
        return $this->hasOne(SptPemeriksaanBphtbModel::class, 't_idspt', 't_idspt')->withDefault();
    }

    public function dataRefnum()
    {
        return $this->hasOne(PembayaranBphtbModel::class, 't_idspt', 't_idspt')->withDefault();
    }
}
