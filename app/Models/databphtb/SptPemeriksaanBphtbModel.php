<?php

namespace App\Models\databphtb;

use App\Models\databphtb\SptBphtbModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SptPemeriksaanBphtbModel extends Model
{
    use HasFactory;

    protected $connection = 'bphtb';
    protected $table    = 't_pemeriksaan';
    protected $primaryKey = 't_idpemeriksa';
    protected $fillable = ['t_luastanah_pemeriksa', 't_luasbangunan_pemeriksa', 't_npop_bphtb_pemeriksa'];

    public function SptBphtbModel(){
		return $this->belongsTo(SptBphtbModel::class, 't_idspt', 't_idspt')->withDefault(function ($data, $post) {
            $data->t_luastanah_pemeriksa = null;
            $data->t_luasbangunan_pemeriksa = null;
            $data->t_npop_bphtb_pemeriksa = null;
        });
	}
}
