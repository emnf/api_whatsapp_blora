<?php

namespace App\Models\databphtb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBphtbModel extends Model
{
    use HasFactory;
    protected $connection = 'bphtb';
    protected $table = 's_users';
    protected $primaryKey = 's_iduser';
}
