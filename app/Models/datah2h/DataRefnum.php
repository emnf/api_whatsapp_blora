<?php

namespace App\Models\datah2h;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DataRefnum extends Model
{
    use HasFactory;

    protected $connection = 'api_wa';
    protected $table    = 't_data';
    protected $primaryKey = 'id';
    protected $fillable = [
        't_idspt',
        't_id_transaksi',
        't_nop',
        't_denda',
        't_total',
        't_pokok',
        't_idjenispembayaran',
        't_tahunspt',
        't_dari_rekening',
        't_ke_rekening',
        't_refnum',
        'created_at',
        'updated_at'
    ];

    public static function updateDataRefnum($bphtb = null, $pbb = null)
    {
        if ($bphtb != null) {
            DataRefnum::where([
                't_idspt' => $bphtb->t_idspt,
                't_tahunspt' => $bphtb->t_periodespt,
            ])->update(array(
                't_refnum' => Str::random(8)
            ));

            return DataRefnum::where([['t_idspt', $bphtb->t_idspt], ['t_tahunspt', $bphtb->t_periodespt]])->first();
        } else if ($pbb != null) {
            DataRefnum::where([
                ['t_nop', $pbb['nop']],
                ['t_tahunspt', $pbb['tax_year']]
            ])->update(array(
                't_refnum' => Str::random(8)
            ));

            return DataRefnum::where([['t_nop', $pbb['nop']], ['t_tahunspt', $pbb['tax_year']]])->first();
        } else {
        }
    }
}
