<?php

namespace App\Models\datah2h;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class h2hLogModel extends Model
{
    use HasFactory;

    protected $connection = 'api_wa';
    protected $table    = 'h2h_log';
    protected $primaryKey = 'id';
    protected $fillable = ['kode', 'akses', 'url', 'message', 'created_at', 'updated_at'];
}
