<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatObjekPajak extends Model
{
    use HasFactory;


    protected $connection = 'oracle';
    protected $table = 'DAT_OBJEK_PAJAK';
    protected $primaryKey =
    [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
    ];

    public $incrementing = false;
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'SUBJEK_PAJAK_ID',
        'JALAN_OP',
        'BLOK_KAV_NO_OP',
        'RW_OP',
        'RT_OP',
        'KD_BANK_PERSEPSI',
        'KD_TP',
        'DENDA_SPPT',
        'JML_SPPT_YG_DIBAYAR',
        'TGL_PEMBAYARAN_SPPT',
        'TGL_REKAM_BYR_SPPT',
        'NIP_REKAM_BYR_SPPT'
    ];
}
