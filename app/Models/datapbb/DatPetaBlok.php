<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatPetaBlok extends Model
{
    use HasFactory;
    protected $connection = 'oracle';
    protected $table = 'DAT_PETA_BLOK';
    public $timestamps = false;
}
