<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistObjekPajak extends Model
{
    use HasFactory;

    protected $connection = 'oracle';
    protected $table = 'HIS_OBJEK_PAJAK';
}
