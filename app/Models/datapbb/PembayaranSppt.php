<?php

namespace App\Models\datapbb;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PembayaranSppt extends Model
{
    use HasFactory;

    protected $connection = 'oracle';
    public $timestamps = false;
    protected $table = 'PEMBAYARAN_SPPT';
    protected $primaryKey =   
    [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'THN_PAJAK_SPPT',
        'PEMBAYARAN_SPPT_KE',
        'KD_KANWIL',
        'KD_KANTOR',
        'KD_TP'
    ];

    public $incrementing = false;
    protected $fillable = [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'THN_PAJAK_SPPT',
        'PEMBAYARAN_SPPT_KE',
        'KD_KANWIL_BANK',
        'KD_KPPBB_BANK',
        'KD_BANK_TUNGGAL',
        'KD_BANK_PERSEPSI',
        'KD_TP',
        'DENDA_SPPT',
        'JML_SPPT_YG_DIBAYAR',
        'TGL_PEMBAYARAN_SPPT',
        'TGL_REKAM_BYR_SPPT',
        'NIP_REKAM_BYR_SPPT'
    ];

    public static function whereByNop($nop)
    {
        return PembayaranSppt::where([
                ['KD_PROPINSI', '=', $nop['KD_PROPINSI']],
                ['KD_DATI2', '=', $nop['KD_DATI2']],
                ['KD_KECAMATAN', '=', $nop['KD_KECAMATAN']],
                ['KD_KELURAHAN', '=', $nop['KD_KELURAHAN']],
                ['KD_BLOK', '=', $nop['KD_BLOK']],
                ['NO_URUT', '=', $nop['NO_URUT']],
                ['KD_JNS_OP', '=', $nop['KD_JNS_OP']],
                ['KD_JNS_OP', '=', $nop['KD_JNS_OP']],
                ['THN_PAJAK_SPPT', '=', $nop['THN_PAJAK_SPPT']]
        ]);
    }

    public static function createData($nop,$data,$denda){
        return PembayaranSppt::create([
            'KD_PROPINSI' => $nop['KD_PROPINSI'],
            'KD_DATI2' => $nop['KD_DATI2'],
            'KD_KECAMATAN' => $nop['KD_KECAMATAN'],
            'KD_KELURAHAN' => $nop['KD_KELURAHAN'],
            'KD_BLOK' => $nop['KD_BLOK'],
            'NO_URUT' => $nop['NO_URUT'],
            'KD_JNS_OP' => $nop['KD_JNS_OP'],
            'THN_PAJAK_SPPT' => $nop['THN_PAJAK_SPPT'],
            'PEMBAYARAN_SPPT_KE' => 1,
            'KD_KANWIL_BANK' => '29',
            'KD_KPPBB_BANK' => '09',
            'KD_BANK_TUNGGAL' => '99',
            'KD_BANK_PERSEPSI' => '99',
            'KD_TP' => '99',
            'DENDA_SPPT' => $denda['denda'],
            'JML_SPPT_YG_DIBAYAR' => $denda['total'],
            'TGL_PEMBAYARAN_SPPT' => date('Y-m-d'),
            'TGL_REKAM_BYR_SPPT' => date('Y-m-d h:i:s'),
            'NIP_REKAM_BYR_SPPT' => env('NIP_REKAM_BYR_SPPT')
        ]);
    }
}
