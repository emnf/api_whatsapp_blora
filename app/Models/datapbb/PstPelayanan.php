<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PstPelayanan extends Model
{
    use HasFactory;
    protected $connection = 'oracle';
    protected $table = 'PST_PERMOHONAN';
    public $timestamps = false;
}
