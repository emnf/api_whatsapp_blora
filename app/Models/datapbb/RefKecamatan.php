<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefKecamatan extends Model
{
    use HasFactory;
    protected $connection = 'oracle';
    protected $table = 'REF_KECAMATAN';
    public $timestamps = false;
}
