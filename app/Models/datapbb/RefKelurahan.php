<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefKelurahan extends Model
{
    use HasFactory;
    protected $connection = 'oracle';
    protected $table = 'REF_KELURAHAN';
    public $timestamps = false;
}
