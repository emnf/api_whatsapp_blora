<?php

namespace App\Models\datapbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sppt extends Model
{
    use HasFactory;
    protected $connection = 'oracle';
    protected $table = 'SPPT';
    public $timestamps = false;
    protected $primaryKey =
    [
        'KD_PROPINSI',
        'KD_DATI2',
        'KD_KECAMATAN',
        'KD_KELURAHAN',
        'KD_BLOK',
        'NO_URUT',
        'KD_JNS_OP',
        'THN_PAJAK_SPPT'
    ];

    public $incrementing = false;
    protected $fillable = [
        'STATUS_PEMBAYARAN_SPPT'
    ];

    public static function getDataByNop($nop)
    {
        return Sppt::select(
            'SPPT.*',
            'REF_DATI2.NM_DATI2',
            'DAT_SUBJEK_PAJAK.NM_WP',
            'DAT_OBJEK_PAJAK.JALAN_OP',
            'DAT_OBJEK_PAJAK.RW_OP',
            'DAT_OBJEK_PAJAK.RT_OP',
            'REF_KECAMATAN.NM_KECAMATAN AS OP_KECAMATAN',
            'REF_KELURAHAN.NM_KELURAHAN AS OP_KELURAHAN'
        )
            ->leftJoin('REF_DATI2', [
                ['REF_DATI2.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_DATI2.KD_DATI2', '=', 'SPPT.KD_DATI2']
            ])
            ->leftJoin('REF_KECAMATAN', [
                ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
            ])
            ->leftJoin('REF_KELURAHAN', [
                ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
            ])
            ->leftJoin('DAT_OBJEK_PAJAK', [
                ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'SPPT.KD_BLOK'],
                ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'SPPT.NO_URUT'],
                ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'SPPT.KD_JNS_OP']
            ])
            ->leftJoin('DAT_SUBJEK_PAJAK', [
                ['DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID']
            ])
            ->where([
                ['SPPT.KD_PROPINSI', '=', $nop['KD_PROPINSI']],
                ['SPPT.KD_DATI2', '=', $nop['KD_DATI2']],
                ['SPPT.KD_KECAMATAN', '=', $nop['KD_KECAMATAN']],
                ['SPPT.KD_KELURAHAN', '=', $nop['KD_KELURAHAN']],
                ['SPPT.KD_BLOK', '=', $nop['KD_BLOK']],
                ['SPPT.NO_URUT', '=', $nop['NO_URUT']],
                ['SPPT.KD_JNS_OP', '=', $nop['KD_JNS_OP']],
                ['SPPT.KD_JNS_OP', '=', $nop['KD_JNS_OP']],
                ['SPPT.THN_PAJAK_SPPT', '=', $nop['THN_PAJAK_SPPT']]
            ]);
    }

    public static function updatePembayaranSppt($nop, $status)
    {
        return Sppt::where([
            ['KD_PROPINSI', $nop['KD_PROPINSI']],
            ['KD_DATI2', $nop['KD_DATI2']],
            ['KD_KECAMATAN', $nop['KD_KECAMATAN']],
            ['KD_KELURAHAN', $nop['KD_KELURAHAN']],
            ['KD_BLOK', $nop['KD_BLOK']],
            ['NO_URUT', $nop['NO_URUT']],
            ['KD_JNS_OP', $nop['KD_JNS_OP']],
            ['THN_PAJAK_SPPT', $nop['THN_PAJAK_SPPT']],
        ])->update(array(
            'STATUS_PEMBAYARAN_SPPT' => $status
        ));
    }

    public function getKodeKec($kecamatan)
    {
        $kec = strtoupper($kecamatan);
        $data = DB::table('REF_KECAMATAN')->where('NM_KECAMATAN', '=', $kec)->first();
        return $data;
    }

    public function hitungDenda()
    {
        $parameter1 = '2023-01-01'; // Replace with your actual date
        $parameter2 = 1000; // Replace with your actual number

        $result = DB::select(

            'SELECT HITUNG_DENDA(:tgljatuhtempo, :pajakYgHrsDibayar) as denda',
            ['tgljatuhtempo' => $parameter1, 'pajakYgHrsDibayar' => $parameter2]
        );

        // Access the result
        $resultValue = $result[0]->result;
        dd($resultValue);
    }
}
