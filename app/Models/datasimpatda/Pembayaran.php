<?php

namespace App\Models\datasimpatda;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\datasimpatda\RekeningView;
use App\Models\datasimpatda\TransaksiSimpatda;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pembayaran extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 't_pembayaran';
    protected $fillable = [
        'uuid',
        't_no_pembayaran',
        't_tgl_pembayaran',
        't_id_transaksi',
        't_id_via_pembayaran',
        't_id_rekening_pembayaran',
        't_id_jenis_pembayaran',
        't_jumlah_pembayaran',
        't_jumlah_bulan_bunga',
        't_id_bank',
        'created_by',
        't_total_bayar'
    ];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['text'];
    protected static $logOnlyDirty = true;

    public function RekeningView()
    {
        return $this->belongsTo(RekeningView::class, 't_id_rekening_pembayaran');
    }

    public function TransaksiSimpatda()
    {
        return $this->belongsTo(TransaksiSimpatda::class, 't_id_transaksi','id');
    }

    public static function getMaxNoPembayaran($thnpembayaran)
    {
        return Pembayaran::whereRaw("EXTRACT(YEAR FROM t_tgl_pembayaran::date) = $thnpembayaran ")->max('t_no_pembayaran') + 1;
    }

    public static function simpanPembayaran($dataInquiry, $hitungDenda, $rekeningDenda)
    {
        $pokok_pembayaran = [
            'uuid' => Uuid::uuid4()->toString(),
            't_no_pembayaran' => Pembayaran::getMaxNoPembayaran(Carbon::now()->format('Y')),
            't_tgl_pembayaran' => Carbon::now()->format('Y-m-d'),
            't_id_transaksi' => $dataInquiry->id,
            't_id_via_pembayaran' => $dataInquiry->channel_id,
            't_id_rekening_pembayaran' => $dataInquiry->t_id_rekening,
            't_id_jenis_pembayaran' => 1, // pokok
            't_jumlah_pembayaran' => $dataInquiry->t_jumlah_pajak * 1,
            'created_by' => 52,
        ];

        $pokok = Pembayaran::create($pokok_pembayaran);

        if ($hitungDenda['denda'] != null) {
            $bunga_pembayaran = [
                'uuid' => Uuid::uuid4()->toString(),
                't_no_pembayaran' => $pokok->t_no_pembayaran,
                't_tgl_pembayaran' => Carbon::now()->format('Y-m-d'),
                't_id_transaksi' => $dataInquiry->id,
                't_id_via_pembayaran' => $dataInquiry->channel_id,
                't_id_rekening_pembayaran' => $rekeningDenda->s_id_korek_bunga,
                't_id_jenis_pembayaran' => 3, //bunga
                't_jumlah_pembayaran' => $hitungDenda['denda'] * 1,
                'created_by' => 52,
            ];
            Pembayaran::create($bunga_pembayaran);
        }

        if ($dataInquiry->t_jumlah_kenaikan != null) {
            $denda_pembayaran = [
                'uuid' => Uuid::uuid4()->toString(),
                't_no_pembayaran' => $pokok->t_no_pembayaran,
                't_tgl_pembayaran' => Carbon::now()->format('Y-m-d'),
                't_id_transaksi' => $dataInquiry->id,
                't_id_via_pembayaran' => $dataInquiry->channel_id,
                't_id_rekening_pembayaran' => $dataInquiry->t_id_rekening,
                't_id_jenis_pembayaran' =>  2, //denda
                't_jumlah_pembayaran' => $dataInquiry->t_jumlah_kenaikan,
                'created_by' => 52,
            ];
            Pembayaran::create($denda_pembayaran);
        }
    }
}
