<?php

namespace App\Models\datasimpatda;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RekeningView extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 'rekening_view';
}
