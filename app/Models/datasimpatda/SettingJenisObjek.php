<?php

namespace App\Models\datasimpatda;
use Illuminate\Database\Eloquent\Model;
use App\Models\datasimpatda\WpObjekView;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SettingJenisObjek extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 's_jenis_objek';
    protected $fillable = ['uuid', 's_kode', 's_nama', 's_nama_singkat', 's_tgl_akhir_lapor', 's_tgl_akhir_bayar', 's_jumlah_hari_tempo', 's_jenis_pungutan', 's_jumlah_bulan_bunga', 'created_by'];

    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = [ 'text'];
    protected static $logOnlyDirty = true;

    public function WpObjekView()
    {
        return $this->belongsToMany(WpObjekView::class, 't_id_jenis_objek', 'id');
    }
}
