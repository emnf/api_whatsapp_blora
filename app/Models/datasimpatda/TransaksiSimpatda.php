<?php

namespace App\Models\datasimpatda;

use App\Models\datasimpatda\Pembayaran;
use Illuminate\Database\Eloquent\Model;
use App\Models\datasimpatda\WpObjekView;
use App\Models\datasimpatda\SettingJenisObjek;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TransaksiSimpatda extends Model
{
    use HasFactory;

    protected $connection = 'simpatda';
    protected $table = 't_transaksi';
    protected $fillable = [
        'uuid', 't_id_objek', 't_id_jenis_objek', 't_id_rekening', 't_id_jenis_surat',
        't_no_pendataan', 't_tgl_pendataan', 't_tahun_pajak', 't_masa_awal', 't_masa_akhir', 't_dasar_pengenaan', 't_tarif_persen',
        't_tarif_dasar', 't_volume', 't_satuan',
        't_tgl_jatuh_tempo', 't_jumlah_pajak',
        't_persen_kenaikan', 't_jumlah_kenaikan',
        't_jumlah_bunga',
        'created_by_pendataan',
        't_no_kontrak',
        't_nilai_kontrak',
        't_ketetapandenda',
        't_nama_kegiatan',
        't_no_penetapan', 't_tgl_penetapan', 'created_by_penetapan', 't_keterangan', 't_kode_bayar',
        't_id_lhp', 't_id_lhp_detail', 'is_esptpd', 'channel_id', 't_id_satker', 't_kompensasi', 't_nppd'
    ];
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['text'];
    protected static $logOnlyDirty = true;

    public function SettingJenisObjek()
    {
        return $this->belongsTo(SettingJenisObjek::class, 't_id_jenis_objek');
    }

    public function Pembayaran()
    {
        return $this->hasMany(Pembayaran::class, 't_id_transaksi')->orderBy('t_id_jenis_pembayaran', 'asc');
    }

    public function WpObjekView()
    {
        return $this->hasOne(WpObjekView::class, 'id', 't_id_objek');
    }
}
