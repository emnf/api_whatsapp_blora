<?php

namespace App\Models\datasimpatda;

use App\Models\datasimpatda\WpView;
use Illuminate\Database\Eloquent\Model;
use App\Models\datasimpatda\SettingJenisObjek;
use App\Models\datasimpatda\TransaksiSimpatda;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WpObjekView extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 'view_wpobjek';

    public function WpView()
    {
        return $this->belongsTo(WpView::class, 't_id_wp', 'id');
    }

    public function SettingJenisObjek()
    {
        return $this->hasOne(SettingJenisObjek::class, 'id', 't_id_jenis_objek');
    }

    public function TransaksiSimpatda()
    {
        return $this->hasMany(TransaksiSimpatda::class, 't_id_objek', 'id');
    }
}
