<?php

namespace App\Models\datasimpatda;

use Illuminate\Database\Eloquent\Model;
use App\Models\datasimpatda\WpObjekView;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WpView extends Model
{
    use HasFactory;
    protected $connection = 'simpatda';
    protected $table = 'view_wp';

    public function WpObjekView()
    {
        return $this->hasMany(WpObjekView::class, 't_id_wp', 'id');
    }
}
