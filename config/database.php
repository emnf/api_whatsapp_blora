<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'api_wa'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [
        'api_wa' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'api_wa'),
            'username' => env('DB_USERNAME', 'postgres'),
            'password' => env('DB_PASSWORD', 'postgres'),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => env('DB_CONNECTIONasd'),
            'database' => env('DB_DATABASEasda', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'bphtb' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST_bphtb', '127.0.0.1'),
            'port' => env('DB_PORT_bphtb', '5432'),
            'database' => env('DB_DATABASE_bphtb', 'bphtb'),
            'username' => env('DB_USERNAME_bphtb', 'postgres'),
            'password' => env('DB_PASSWORD_bphtb', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'simpatda' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST_simpatda', '127.0.0.1'),
            'port' => env('DB_PORT_simpatda', '5432'),
            'database' => env('DB_DATABASE_simpatda', 'simpatda'),
            'username' => env('DB_USERNAME_simpatda', 'postgres'),
            'password' => env('DB_PASSWORD_simpatda', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => env('DB_CONNECTIONasd'),
            'host' => env('DB_HOSTdsad', 'localhost'),
            'port' => env('DB_PORTasd', '1433'),
            'database' => env('DB_DATABASEasd', 'forge'),
            'username' => env('DB_USERNAMEasd', 'forge'),
            'password' => env('DB_PASSWORDasd', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        'oracle' => [
            'driver'         => 'oracle',
            // 'tns'            => env('DB_TNS', ''),
            'service_name'   => env('ORACLE_SERVICE_NAME', ''),
            'host'           => env('DB_HOST_ORACLE', 'localhost'),
            'port'           => env('DB_PORT_ORACLE', '1521'),
            'database'       => env('DB_DATABASE_ORACLE', ''),
            'username'       => env('DB_USERNAME_ORACLE', ''),
            'password'       => env('DB_PASSWORD_ORACLE', ''),
            'charset'        => env('DB_CHARSET', 'AL32UTF8'),
            'prefix'         => env('DB_PREFIX', ''),
            'prefix_schema'  => env('DB_SCHEMA_PREFIX', ''),
            'edition'        => env('DB_EDITION', 'ora$base'),
            'server_version' => env('DB_SERVER_VERSION', '11g'),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'url' => env('DB_CONNECTIONasdas'),
            'host' => env('DB_HOSTasdas', '127.0.0.1'),
            'port' => env('DB_PORTasdas', '3306'),
            'database' => env('DB_DATABASEasdas', 'forge'),
            'username' => env('DB_USERNAMEasdas', 'forge'),
            'password' => env('DB_PASSWORDasdas', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],


    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_') . '_database_'),
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],

    ],

];
