<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('api_wa')->create('jenis_pembayaran', function (Blueprint $table) {
            $table->increments('s_idjenispembayaran');
            $table->string("s_namajenisPembayaran")->nullable();
            $table->timestamps();
        });

        Schema::connection('api_wa')->create('t_data', function (Blueprint $table) {
            $table->id();
            $table->string("t_idspt")->nullable();
            $table->string("t_nop")->nullable();
            $table->integer('t_idjenispembayaran')->nullable();
            $table->foreign('t_idjenispembayaran')->references('s_idjenispembayaran')->on('jenis_pembayaran');
            $table->string("t_tahunspt")->nullable();
            $table->string("t_refnum")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('api_wa')->dropIfExists('t_data');
        Schema::connection('api_wa')->dropIfExists('jenisPembayaran');
    }
}
