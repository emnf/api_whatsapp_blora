<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::connection('api_wa')->table('s_user')->insert([
            [
                'id' => 1,
                'username' => 'blora',
                'password' => Hash::make('bl0r@whatsap2023'),
                'created_at' => now(),
                'updated_at' => now()

            ]
        ]);

        DB::connection('api_wa')->unprepared("ALTER SEQUENCE s_user_id_seq RESTART WITH 2;");

        DB::connection('api_wa')->table('jenis_pembayaran')->insert([
            [
                's_idjenispembayaran' => 1,
                's_namajenisPembayaran' => 'BPHTB',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                's_idjenispembayaran' => 2,
                's_namajenisPembayaran' => 'PBB',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                's_idjenispembayaran' => 3,
                's_namajenisPembayaran' => 'SIMPATDA',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
