<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\BPHTB\BphtbController;
use App\Http\Controllers\PBB\PbbController;
use App\Http\Controllers\PDL\PdlController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| Description of API Whatsapp Kab Blora
|
] Development By emnf ngedit h2h kalteng punya frenandiade@gmail.com
| HEHEHEHE ..
*/

Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum', 'throttle:60,1']], function () {
    Route::prefix('pbb-service')->controller(PbbController::class)->group(function () {
        Route::get('tagihan', 'cekTagihan');
        Route::get('tagihan-wilayah', 'cekTagihanWilayah');
        Route::get('statuspelayanan', 'statuspelayanan');
        Route::get('by-status', 'bystatus');
        Route::get('kecamatan', 'kecamatan');
        Route::get('kelurahan', 'kelurahan');
        Route::get('blok', 'kdblok');
        Route::get('statusbayar', 'statusbayar');
        Route::get('pembayarnpertama', 'pembayarnpertama');
    });

    Route::prefix('bphtb-service')->controller(BphtbController::class)->group(function () {
        Route::post('by-nodaftar', 'permohonanByKodeDaftar');
        Route::post('by-status', 'permohonanByStatus');
        Route::post('by-billing', 'permohonanByBilling');
        Route::post('by-dibayar', 'permohonanByDibayar');
    });

    Route::prefix('pdl-service')->controller(PdlController::class)->group(function () {
        Route::get('tagihan', 'cekTagihan');
        Route::get('tagihan-periode', 'cekTagihanPeridoe');
        Route::get('tagihan-by-tglinput', 'cekTagihanTglPendataan');
        Route::get('tagihan-by-tglbayar', 'cekTagihanTglBayar');
    });

    Route::post('/logout', [AuthController::class, 'logout']);
});
